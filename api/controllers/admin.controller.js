const { Order, AdminUser, Driver, SuperAdmin, Notifications, WebCommission, GhostUser, PushNotification, MyIncome } = require('../models');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { check, validationResult } = require('express-validator');
const { config } = require('../../config/data');
const { Restaurant } = require('../models/restaurant.model');
const stripe = require('stripe')('sk_test_IZLsuRFZK33Px6BfNApdNFAf')
const { token } = require('morgan');
const paypal = require('paypal-rest-sdk');
const firebase = require("./../../config/firebase")
const moment = require("moment");

const {
	orderController,
} = require('./order.controller');
const { request } = require('http');

paypal.configure({
	'mode': 'sandbox',
	'client_id': 'ATE-9_XP3_L0kfbvYmiWsPyW5kNjwdaTBQqSjJClco3CKGelTCiG19-OcJrNRQw4UvGsLWGuGEaiJwp0',
	'client_secret': 'EPed6gO3twc5RwM3H8hQkkicED179GAMjXXAjdZSc9JHC-wkG1JVgpzdgPq6j-KnM_h6iypck-eZ0Hlj'
})

class AdminController {
	async fetchHtml(req, res) {
		try {
			const html = '<!DOCTYPE html>'
				+ '<html><head> </head><body> <p>this is for printer</p> </body></html>';
			// res.writeHead(200, {
			// 	'Content-Type': 'text/html',
			// 	// 'Content-Length': html.length,
			// 	'Expires': new Date().toUTCString()
			// });
			res.json({ html });
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async fetchAllUsers(req, res) {
		try {
			const adminUsers = await AdminUser.find({ type: 'user' }).exec();
			if (!adminUsers) {
				throw new Error('No user found')
			}
			res.json({ adminUsers });
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async fetchAllOwners(req, res) {
		try {
			const adminUsers = await AdminUser.find({ type: 'owner' }).exec();
			if (!adminUsers) {
				throw new Error('No owner found')
			}
			res.json({ adminUsers });
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async fetchAllDrivers(req, res) {
		try {
			const drivers = await Driver.find().exec();
			if (drivers) {
				res.json({ drivers });
			} else {
				throw { drivers, message: 'Error in AdminController.fetchAlldrivers' }
			}
		} catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}
	async fetchAllDevices(req, res) {
		try {
			const devices = await PushNotification.find().exec();
			if (devices) {
				res.json({ devices });
			} else {
				throw { devices, message: 'No Device Found!' }
			}
		} catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}
	async fetchDevicesByUserId(req, res) {
		try {
			const devices = await PushNotification.findOne({ userId: req.params.id }).exec();
			if (devices) {
				res.json(devices);
			} else {
				throw { devices, message: 'No Device Found!' }
			}
		} catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}
	async fetchAllSuperAdmin(req, res) {
		try {
			const superAdmin = await SuperAdmin.find()
				.populate('jobPosition')
				.exec();
			if (!superAdmin) {
				throw new Error("super admin creation failed")
			}
			res.json({ superAdmin });
		} catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}
	async fetchAdminUsersById(req, res) {
		try {
			const adminUser = await AdminUser.findById(req.params.id).populate("favRestaurants").exec();
			if (!adminUser) {
				throw new Error("No User Found")
			}
			res.json({ adminUser });
		} catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}
	async fetchDriversStats(req, res) {
		try {
			const id = req.params.id
			const orders = await Order.find({ driver: id })
				.populate("restaurant")
				.exec();
			let data = []
			let total = 0
			let deliveries = 0
			orders.forEach(element => {
				if (element.distance > 3) {
					const order = {
						charges: element.restaurant.driverChargesOver,
						address: element.restaurant.address,
						restaurant: element.restaurant.name,
						orderNumber: element.orderNumber,
					}
					total = total + parseInt(order.charges)
					deliveries = deliveries + 1
					console.log(order)
					data.push(order)
				} else {
					const order = {
						charges: element.restaurant.driverChargesWithin,
						restaurant: element.restaurant.name,
						orderNumber: element.orderNumber,
					}
					deliveries = deliveries + 1
					total = total + parseInt(order.charges)
					console.log(order)
					data.push(order)

				}
			});
			const finalData = {
				totalEarning: total,
				orderDelivered: deliveries
			}
			res.json(finalData);
		} catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}
	async fetchOwnerById(req, res) {
		try {
			const adminUser = await AdminUser.findById(req.params.id).populate("restaurants").exec();
			if (!adminUser) {
				throw new Error("No User Found")
			}
			res.json({ adminUser });
		} catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}
	async fetchDriverById(req, res) {
		try {
			const driver = await Driver.findById(req.params.id).populate("favRestaurants").exec();
			if (!driver) {
				throw new Error("No Driver Found")
			}
			res.json({ driver });
		} catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}
	async fetchAllFavRestaurants(req, res) {
		try {
			const data = await AdminUser.findById(req.params.id)
				.populate('favRestaurants')
				// .populate({
				//   path: 'favRestaurants',
				//   options: {
				//     sort: '-updatedAt'
				//   }
				// })
				.select('favRestaurants')
				.exec();
			if (!data) {
				throw new Error('Data Not Found')
			}
			res.json({ data });
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async fetchAllDashboardData(req, res) {
		try {
			const users = await AdminUser.find({ isActive: true, type: 'user' }).exec();
			const drivers = await Driver.find({ isActive: true }).exec();
			const restaurants = await Restaurant.find({ isActive: true }).exec();

			// const allRestaurants = await Restaurant.find().exec();
			// const resOrders = allRestaurants.map(async (restaurant) => {
			// 	const data = await Order.find({ restaurant: restaurant._id })
			// 	console.log(data)
			// 	return data
			// })

			const data = {
				activeUsers: users.length,
				activeDrivers: drivers.length,
				activeRestaurants: restaurants.length,
				users,
				drivers,
				restaurants
				// resOrders,
			}
			res.json(data);
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async fetchAllNotifications(req, res) {
		try {
			const data = await Notifications.find()
				.exec();
			if (!data) {
				throw new Error('No notifications found')
			}
			res.json({ data });
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async fetchNotificationById(req, res) {
		try {
			const data = await Notifications.findById(req.params.id)
				.populate("userId")
				.populate("driverId")
				.exec();
			if (!data) {
				throw new Error("Notification Not Found")
			}
			res.json({ data });
		} catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}
	async fetchNotificationByOwnerId(req, res) {
		try {
			const data = await Notifications.find({ userId: req.params.id })
				.exec();
			if (!data) {
				throw new Error("Notification Not Found")
			}
			res.json({ data });
		} catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}
	async fetchNewOwnerNotifications(req, res) {
		try {
			const notifications = await Notifications.find({ userId: req.params.id })
				.exec();
			if (!notifications) {
				throw new Error("Notification Not Found")
			}
			let data = []
			notifications.forEach(element => {
				if (element.isNewAdded === true) {
					data.push(element)
				}
			});
			res.json({ data });
		} catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}
	async fetchNewDriverNotifications(req, res) {
		try {
			const driver = await Driver.find({ _id: req.params.id })
				.populate("restaurants")
				.exec();
			let data = []
			for (let i = 0; i < driver[0].restaurants.length; i++) {
				const notifications = await Notifications.find({ restaurantId: driver[0].restaurants[i]._id, isNewAdded: true })
					.exec();
				// data.push(notifications)
				data = [
					...data,
					...notifications
				]
			}
			console.log(data)
			if (!data) {
				throw new Error("Notification Not Found")
			}

			res.json({ data });
		} catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}
	async fetchRestaurantCommission(req, res) {
		try {
			const data = await WebCommission.find({ userType: 'restaurant' })
				.exec();
			if (!data) {
				throw new Error("Commossion Not Found")
			}
			res.json({ data });
		} catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}
	async fetchDriverCommission(req, res) {
		try {
			const data = await WebCommission.find({ userType: 'driver' })
				.exec();
			if (!data) {
				throw new Error("Commossion Not Found")
			}
			res.json({ data });
		} catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}
	async fetchNotificationByDriverId(req, res) {
		try {
			const data = await Notifications.find({ driverId: req.params.id })
				.exec();
			if (!data) {
				throw new Error("Notification Not Found")
			}
			res.json({ data });
		} catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}
	async fetchRestaurantRevenue(req, res) {
		try {
			const data = await WebCommission.find({ userId: req.params.id })
				.exec();
			if (!data) {
				throw new Error("Revenue Not Found")
			}
			res.json({ data });
		} catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}
	async fetchMyIncome(req, res) {
		try {
			const data = await MyIncome.find({ userId: req.params.id })
				.exec();
			if (!data) {
				throw new Error("My Income Not Found")
			}
			res.json({ data });
		} catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}
	async fetchNotificationBySuperAdmin(req, res) {
		try {
			const type = req.query.type
			const data = await Notifications.find({ type })
				.exec();
			if (!data.length > 0) {
				throw new Error("Notifications Not Found")
			}
			res.json({ data });
		} catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}
	async fetchNewNotificationBySuperAdmin(req, res) {
		try {
			const data = await Notifications.find({ type: "superadmin", isNewAdded: true })
				.exec();
			if (!data.length > 0) {
				throw new Error("Notifications Not Found")
			}
			res.json({ data });
		} catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}

	// create
	async signUp(req, res) {
		const { type, firstName, lastName, email, password, mobile } = req.body;
		try {
			let user = await AdminUser.findOne({ email, type });
			if (user) {
				throw new Error('User Already Exists');
			}

			user = new AdminUser({
				type, firstName, lastName, email, mobile, password
			});

			//bcrypt password
			const salt = await bcrypt.genSalt(10);
			user.password = await bcrypt.hash(password, salt);
			await user.save();
			let descriptionType = 'user';
			if (user.type === 'owner') descriptionType = 'owner';
			let notificationBody = {
				title: email,
				description: `created ${descriptionType} account`,
				type: 'superadmin',
				userId: user._id
			}
			const notification = new Notifications(notificationBody);
			await notification.save();
			if (!notification) {
				return res.status(400).json({ msg: 'Notification not sent!' });
			}
			res.status(200).json({ user });
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async ghostSignUp(req, res) {
		const { name, mobile } = req.body;
		try {
			let password = mobile
			let email = mobile
			let type = "user"

			let user = await GhostUser.findOne({ email });
			if (user) {
				throw new Error('User Already Exists');
			}

			user = new GhostUser({
				email, name, type, mobile, password
			});

			//bcrypt password
			const salt = await bcrypt.genSalt(10);
			user.password = await bcrypt.hash(password, salt);
			await user.save();
			res.status(200).json({ user });
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async signUpSuperAdmin(req, res) {
		const { email, password } = req.body;
		try {
			let user = await SuperAdmin.findOne({ email });
			if (user) {
				throw new Error('User Already Exists');
			}
			user = new SuperAdmin(req.body);

			//bcrypt password
			const salt = await bcrypt.genSalt(10);
			user.password = await bcrypt.hash(password, salt);

			await user.save();
			res.status(200).json({ superAdmin: user });
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async driverSignUp(req, res) {
		try {
			let driver = await Driver.findOne({ email: req.body.email });
			if (driver) {
				throw new Error('driver Already Exists');
			}
			driver = new Driver(req.body);
			//bcrypt password
			const salt = await bcrypt.genSalt(10);
			driver.password = await bcrypt.hash(req.body.password, salt);

			await driver.save();
			let notificationBody = {
				title: 'New account',
				description: `created driver account`,
				type: 'superadmin',
				driverId: driver._id
			}
			const notification = new Notifications(notificationBody);
			await notification.save();
			if (!notification) {
				return res.status(400).json({ msg: 'Notification not sent!' });
			}
			res.status(200).json({ driver });
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async addDevice(req, res) {
		try {
			let deviceInfo = await PushNotification.findOne({ userId: req.body.userId });
			if (deviceInfo) {
				const result = await PushNotification.findByIdAndUpdate(deviceInfo._id, req.body, {
					new: true
				});
				res.json({ deviceInfo: result });
			} else {
				deviceInfo = new PushNotification(req.body);
				await deviceInfo.save();
				res.status(200).json({ deviceInfo });
			}
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async base64Upload(req, res) {
		try {
			const rawData = req.body.base64

			var base64Data = rawData.replace(/^data:image\/png;base64,/, "");

			const date = new Date;
			const imageName = moment(date).format("YYYYMMDDHHMMSS") + ".jpg"

			require("fs").writeFile("public/" + imageName, base64Data, 'base64', function (err) {
				if (err) {
					res.json({ err })
				}
				res.json({ image: "public/" + imageName })
			});

		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async createDriverNotification(req, res) {
		try {
			const body = req.body
			const itemsRef = firebase.database().ref(`${body.userId}`);
			const newId = itemsRef.push(body.notification).getKey(); // add notification and get its id
			const getItems = firebase.database().ref(`${body.userId}`);
			getItems.on('value', (snapshot) => {
				let items = snapshot.val();
				let newState = [];
				for (let item in items) {
					newState.push({
						id: item,
						title: items[item].title,
						userId: items[item].userId
					});
				}
				setTimeout(
					() => newState.map((item) => {
						if (item.id === newId) {
							const removeItems = firebase.database().ref(`/${body.userId}/${item.id}`);
							removeItems.remove();
						}
					}),
					10000
				);
			});

			const notification = new Notifications(body);
			await notification.save();
			if (!notification) {
				return res.status(400).json({ msg: 'Notification not sent!' });
			}
			res.status(200).json({ result: "success" });
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async login(req, res) {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}
		const { email, password, type } = req.body;
		try {
			let user = await AdminUser.findOne({ email, type });
			if (!user) {
				throw new Error('Invalid Credentials');
			}
			const isMatch = await bcrypt.compare(password, user.password);
			if (!isMatch) {
				throw new Error('Invalid Credentials');
			}
			if (!user.isActive == true) {
				throw new Error('Your Account is disabled, Please Contact Us')
			}
			const payload = {
				user: {
					id: user.id
				}
			};

			jwt.sign(payload, config, { expiresIn: 36000 }, (err, token) => {
				if (err) throw err;
				res.json({ token, user });
			});
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async ghostLogin(req, res) {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}
		const { name, mobile } = req.body;
		try {
			let password = mobile
			let email = mobile
			let type = "user"

			let user = await AdminUser.findOne({ email });
			if (user) {
				let guestUser = await AdminUser.findOne({ email });
				const isMatch = await bcrypt.compare(password, user.password);
				if (!isMatch) {
					throw new Error('Invalid Credentials');
				}
				if (!user.isActive == true) {
					throw new Error('Your Account is disabled, Please Contact Us')
				}
				const payload = {
					user: {
						id: user.id
					}
				};
				jwt.sign(payload, config, { expiresIn: 36000 }, (err, token) => {
					if (err) throw err;
					res.json({ token, guestUser });
				});
			} else {

				let newUser = new AdminUser({
					email, name, type, mobile, password
				});
				//bcrypt password
				const salt = await bcrypt.genSalt(10);
				newUser.password = await bcrypt.hash(password, salt);
				await newUser.save();

				let guestUser = await AdminUser.findOne({ email });
				const isMatch = await bcrypt.compare(password, newUser.password);
				if (!isMatch) {
					throw new Error('Invalid Credentials');
				}
				if (!newUser.isActive == true) {
					throw new Error('Your Account is disabled, Please Contact Us')
				}
				const payload = {
					newUser: {
						id: newUser.id
					}
				};

				jwt.sign(payload, config, { expiresIn: 36000 }, (err, token) => {
					if (err) throw err;
					res.json({ token, guestUser });
				});
			}
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async loginSuperAdmin(req, res) {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}
		const { email, password } = req.body;
		try {
			let user = await SuperAdmin.findOne({ email });
			if (!user) {
				throw new Error('Invalid Credentials');
			}

			const isMatch = await bcrypt.compare(password, user.password);
			if (!isMatch) {
				throw new Error('Invalid Credentials');
			}

			if (!user.isActive == true) {
				throw new Error('Your Account is disabled, Please Contact Us')
			}

			const payload = {
				user: {
					id: user.id
				}
			};

			jwt.sign(payload, config, { expiresIn: 36000 }, (err, token) => {
				if (err) throw err;
				res.json({ token, user });
			});
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async driverLogin(req, res) {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}
		const { email, password } = req.body;
		try {
			let driver = await Driver.findOne({ email });
			if (!driver) {
				throw new Error('Invalid Credentials');
			}

			const isMatch = await bcrypt.compare(password, driver.password);
			if (!isMatch) {
				throw new Error('Invalid Credentials');
			}

			if (!driver.isActive == true) {
				throw new Error('Your Account is disabled, Please Contact Us')
			}
			const payload = {
				driver: {
					id: driver.id
				}
			};

			jwt.sign(payload, config, { expiresIn: 36000 }, (err, token) => {
				if (err) throw err;
				res.json({ token, driver });
			});
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async stripePayment(req, res) {
		try {
			const { cardNumber, month, year, cvc, grandTotal, name } = req.body;
			const token = await stripe.tokens.create({
				card: {
					number: cardNumber,
					exp_month: month,
					exp_year: year,
					cvc,
					name
				},
			});

			const session = await stripe.charges.create({
				amount: grandTotal * 100,
				currency: "GBP",
				description: "Checkout",
				source: token.id,
			});
			res.json(session)
		}
		catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async paypalPayment(req, res) {
		try {
			const { name, price, quantity, description } = req.body;
			const create_payment_json = {
				"intent": "sale",
				"payer": {
					"payment_method": "paypal"
				},
				"redirect_urls": {
					"return_url": `https://foodapps.uk/order-recieved`,
					"cancel_url": 'https://foodapps.uk/'

				},
				"transactions": [{
					"item_list": {
						"items": [{
							"name": name,
							"sku": "001",
							"price": price,
							"currency": "GBP",
							"quantity": quantity
						}]
					},
					"amount": {
						"currency": "GBP",
						"total": price
					},
					"description": description
				}]
			};

			paypal.payment.create(create_payment_json, function (error, payment) {
				if (error) {
					console.log(error)
					throw new Error(error);
				} else {
					console.log(payment)
					for (let i = 0; i < payment.links.length; i++) {
						if (payment.links[i].rel === 'approval_url') {
							res.json({ data: payment.links[i].href });
						}
					}
				}
			});
		}
		catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async createNotification(req, res) {
		try {
			const notification = new Notifications(req.body);
			await notification.save();
			if (!notification) {
				return res.status(400).json({ msg: 'Notification not sent!' });
			}
			const ownerNotifi = firebase.database().ref(`${req.body.userId}`);
			const ownerNotifiId = ownerNotifi.push(req.body).getKey(); // add notification and get its id
			const ownerNotifiItems = firebase.database().ref(`${req.body.userId}`);
			ownerNotifiItems.on('value', (snapshot) => {
				let items = snapshot.val();
				let newState = [];
				for (let item in items) {
					newState.push({
						id: item,
						title: items[item].title,
						userId: items[item].userId
					});
				}
				setTimeout(
					() => newState.map((item) => {
						if (item.id === ownerNotifiId) {
							const removeItems = firebase.database().ref(`/${req.body.userId}/${item.id}`);
							removeItems.remove();
						}
					}),
					10000
				);
			});
			res.json({ notification });
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}

	// update
	async updateUser(req, res) {
		try {
			const result = await AdminUser.findByIdAndUpdate(req.params.id, req.body, {
				new: true
			});
			res.json({ result });
		} catch (error) {
			console.error(error.message);
			res.status(500).json({ error: error.message });
		}
	}
	async updateDriver(req, res) {
		try {
			const result = await Driver.findByIdAndUpdate(req.params.id, req.body, {
				new: true
			});
			res.json({ result });
		} catch (error) {
			console.error(error.message);
			res.status(500).json({ error: error.message });
		}
	}
	async addFavoriteRestaurants(req, res) {
		try {
			const { favRestaurants } = req.body;
			const data = await AdminUser.find({ "favRestaurants.2": { "$exists": true } }).exec();
			let restId = await AdminUser.findById(req.params.id).select('favRestaurants').exec();
			restId = restId.favRestaurants;
			const isFav = restId.includes(favRestaurants)
			let result;
			if (!isFav) {
				if (!data.length > 0) {
					result = await AdminUser.findByIdAndUpdate({ _id: req.params.id }, { $addToSet: { favRestaurants } }, { new: true }).exec();
				} else {
					result = await AdminUser.findByIdAndUpdate({ _id: req.params.id }, { $addToSet: { favRestaurants } }, { new: true }).exec();
					result = await AdminUser.findByIdAndUpdate({ _id: req.params.id }, { $pop: { "favRestaurants": -1 } }, { new: true }).exec();
				}
			}
			res.json({ result });
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async updateNotification(req, res) {
		try {
			const notificationUpdate = await Notifications.findByIdAndUpdate(req.params.id, req.body, {
				new: true
			}).exec();
			if (!notificationUpdate) {
				throw new Error('Notifications did not update.');
			}
			res.json({ data: notificationUpdate });
		}
		catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}

	async uploadImage(req, res) {
		try {
			res.send(req.file.path);
		}
		catch (error) {
			res.status(500).json({ error: error.message });
		}
	}

	async removeUserById(req, res) {
		try {
			const done = await AdminUser.deleteOne({ _id: req.params.id });
			if (!done) {
				throw new Error("Deletion failed, please provide correct id")
			}
			res.send({ deleted: 'Deleted Successfuly.' })
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}

	async deleteNotification(req, res) {
		try {
			const done = await Notifications.deleteOne({ _id: req.params.id });
			if (!done) {
				throw new Error("Deletion failed, please provide correct id")
			}
			res.send({ deleted: 'Deleted Successfuly.' })
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async removeDriverById(req, res) {
		try {
			const done = await Driver.deleteOne({ _id: req.params.id });
			if (!done) {
				throw new Error("Deletion failed, please provide correct id")
			}
			res.send({ deleted: 'Deleted Successfuly.' })
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}

}

const authValidations = {
	signUp: [
		check('name', 'Name is required')
			.not()
			.isEmpty(),
		check('email', 'please include a valid Email').isEmail(),
		check(
			'password',
			'please include correct password of 6 or more characters'
		).isLength({
			min: 6
		})
	],
	login: [
		check('email', 'please include a valid Email').isEmail(),
		check('password', 'Password is required').exists()
	]
};

const adminController = new AdminController()

module.exports = {
	adminController,
	authValidations,
};
