const { Category, Food, SpecialOffer, Cart } = require('../models');
const { Restaurant } = require('../models/restaurant.model');


class FoodController {
  // =====================> CREATE <===================== //
  async createFood(req, res) {
    try {
      const food = new Food(req.body);
      await food.save();
      if (!food) {
        return res.status(400).json({ msg: 'food didnt added' });
      }
      const addFood = await Category.updateOne({ _id: req.body.categoryId },
        { $push: { foodItems: food._id } }).exec();
      if (addFood) {
        const data = await Category.findById(req.body.categoryId)
          .populate('foodItems').exec();
        res.json({ data });
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async addToCart(req, res) {
    try {
      const cart = await Cart.find({ user: req.body.user }).exec()
      if (cart.length) {


        cart.forEach(async element => {
          const condition = "" + element.restaurant !== req.body.restaurant
          if (condition) {
            const done = await Cart.deleteOne({ _id: element._id }).exec();
          }
        });


        let index = cart.findIndex(obj => obj.foodItems._id === req.body.foodItems._id)
        if (index === -1) {
          const data = {
            user: req.body.user,
            count: 1,
            foodItems: req.body.foodItems,
            restaurant: req.body.restaurant,
            price: req.body.foodItems.option ? parseFloat(req.body.foodItems.price) + parseFloat(req.body.foodItems.option.price) : req.body.foodItems.price
          }
          const food = new Cart(data);
          await food.save();
          res.json(food)
          if (!food) {
            return res.status(400).json({ msg: 'Not added to cart' });
          }
        } else {
          if (req.body.foodItems.option) {
            if (cart[index].foodItems.option._id === req.body.foodItems.option._id) {
              const products = {
                count: parseInt(cart[index].count) + 1,
              }
              const cartAdded = await Cart.findByIdAndUpdate(cart[index]._id, products, {
                new: true
              }).exec();
              res.json(cartAdded)
            } else {
              const data = {
                user: req.body.user,
                count: 1,
                foodItems: req.body.foodItems,
                restaurant: req.body.restaurant,
                price: req.body.foodItems.option ? parseFloat(req.body.foodItems.price) + parseFloat(req.body.foodItems.option.price) : req.body.foodItems.price
              }
              const food = new Cart(data);
              await food.save();
              res.json(food)
              if (!food) {
                return res.status(400).json({ msg: 'Not added to cart' });
              }
            }
          } else {
            const products = {
              count: parseInt(cart[index].count) + 1,
            }
            const cartAdded = await Cart.findByIdAndUpdate(cart[index]._id, products, {
              new: true
            }).exec();
            res.json(cartAdded)
          }

        }
      } else {
        const data = {
          user: req.body.user,
          count: 1,
          foodItems: req.body.foodItems,
          restaurant: req.body.restaurant,
          price: req.body.foodItems.option ? parseFloat(req.body.foodItems.price) + parseFloat(req.body.foodItems.option.price) : req.body.foodItems.price
        }
        const food = new Cart(data);
        await food.save();
        res.json(food)
        if (!food) {
          return res.status(400).json({ msg: 'Not added to cart' });
        }
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async subFromCart(req, res) {
    try {
      const id = req.params.id
      const cart = await Cart.findById(id).exec()
      if (cart.count > 1) {
        const products = {
          count: parseInt(cart.count) - 1,
        }
        const cartAdded = await Cart.findByIdAndUpdate(id, products, {
          new: true
        }).exec();
        res.json(cartAdded)
      } else {
        const done = await Cart.deleteOne({ _id: id }).exec();
        if (done) {
          res.json({ result: 'Deleted from cart' })
        }
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }

  async createCategory(req, res) {
    try {
      const category = new Category(req.body);
      await category.save();
      if (!category) {
        return res.status(400).json({ msg: 'Category didnt added' });
      }
      const addCategory = await Restaurant.updateOne({ _id: req.body.restaurantId },
        { $push: { categories: category._id } }).exec();
      if (addCategory) {
        const data = await Restaurant.findById(req.body.restaurantId)
          .populate('categories').exec();
        res.json({ data: category });
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async createPackage(req, res) {
    try {
      const offer = new SpecialOffer(req.body);
      await offer.save();
      if (!offer) {
        return res.status(400).json({ msg: 'Special Offer didnt added' });
      }
      const addPackage = await Restaurant.updateOne({ _id: req.body.restaurant },
        { $push: { packages: offer._id } }).exec();
      if (addPackage) {
        const data = await Restaurant.findById(req.body.restaurant)
          .populate('packages').exec();
        res.json({ data });
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async fetchFood(req, res) {
    try {
      const data = await Food.findById(req.params.id).exec();
      if (data) {
        res.json({ data });
      } else {
        throw new Error('no Data');
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async fetchAllFood(req, res) {
    try {
      const data = await Food.find().exec();
      if (data) {
        res.json({ data });
      } else {
        throw new Error('no Data');
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async fetchAllCarts(req, res) {
    try {
      const data = await Cart.find().exec();
      if (data) {
        res.json({ data });
      } else {
        throw new Error('no Data');
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async fetchMyCart(req, res) {
    try {
      const data = await Cart.find({ user: req.params.id })
        // .populate("foodItems")
        .exec();
      if (data) {
        res.json({ data });
      } else {
        throw new Error('no Data');
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async fetchRestaurantCart(req, res) {
    try {
      const data = await Cart.find({ user: req.params.id, restaurant: req.query.restaurant })
        // .populate("foodItems")
        .exec();
      if (data) {
        res.json({ data });
      } else {
        throw new Error('no Data');
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async fetchCategoryWithActiveFoods(req, res) {
    try {
      const category = await Category.findById(req.params.id).populate('foodItems').exec();
      if (category) {
        let data = {
          _id: category._id,
          name: category.name,
          foodItems: []
        }
        category.foodItems.forEach(element => {
          if (element.isActive) {
            data.foodItems.push(element)
          }
        });
        res.json({ data });
      } else {
        res.status(500).json({ error: 'No Data' });
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }

  async fetchCategory(req, res) {
    try {
      const data = await Category.findById(req.params.id).populate('foodItems').exec();
      if (data) {
        res.json({ data });
      } else {
        throw new Error('no Data');
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }

  async fetchPackage(req, res) {
    try {
      const data = await SpecialOffer.find({ restaurant: req.params.id }).exec();
      if (data) {
        res.json({ data });
      } else {
        throw new Error('no Data');
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }

  async fetchPackageActive(req, res) {
    try {
      const data = await SpecialOffer.find({ restaurant: req.params.id, isActive: true }).exec();
      if (data) {
        res.json({ data });
      } else {
        throw new Error('no Data');
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async fetchAllCategory(req, res) {
    try {
      const data = await Category.find().exec();
      if (data) {
        res.json({ data });
      } else {
        throw new Error('no Data');
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async updateFood(req, res) {
    try {
      const foodUpdate = await Food.findByIdAndUpdate(req.params.id, req.body, {
        new: true
      }).exec();
      if (foodUpdate) {
        res.json({ data: foodUpdate });
      } else {
        throw new Error(
          'Error: in  FoodController => updateFood'
        );
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async updatePackage(req, res) {
    try {
      const foodUpdate = await SpecialOffer.findByIdAndUpdate(req.params.id, req.body, {
        new: true
      }).exec();
      if (foodUpdate) {
        res.json({ data: foodUpdate });
      } else {
        throw new Error(
          'Error: in  FoodController => updatePckage'
        );
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async updateCategory(req, res) {
    try {
      const categoryUpdate = await Category.findByIdAndUpdate(req.params.id, req.body, {
        new: true
      }).exec();
      if (categoryUpdate) {
        res.json({ data: categoryUpdate });
      } else {
        throw new Error(
          'Error: in  FoodController => updateCategory'
        );
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async addFoodToCategory(req, res) {
    try {
      const { foodItems } = req.body;
      const categoryUpdate = await Category.findByIdAndUpdate(req.params.id, { $push: { foodItems } }, {
        new: true
      }).exec();
      if (categoryUpdate) {
        res.json({ data: categoryUpdate });
      } else {
        throw new Error(
          'Error: in  FoodController => updateCategory'
        );
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async removeFood(req, res) {
    try {
      const done = await Food.deleteOne({ _id: req.params.id }).exec();
      if (done) {
        res.json('Food Deleted')
      } else {
        throw { message: "Error: FoodController.removeFood", done }
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async removeFromCart(req, res) {
    try {
      const done = await Cart.deleteOne({ _id: req.params.id }).exec();
      if (done) {
        res.json({ result: 'Removed From Cart!' })
      } else {
        throw { message: "Error: FoodController.removeFromCart", done }
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async removePackage(req, res) {
    try {
      const done = await SpecialOffer.deleteOne({ _id: req.params.id }).exec();
      if (done) {
        res.json({ result: 'Package Deleted' })
      } else {
        throw { message: "Error: FoodController.removePackage", done }
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async removeCategory(req, res) {
    try {
      const done = await Category.deleteOne({ _id: req.params.id }).exec();
      if (done) {
        res.json('Food Category Deleted')
      } else {
        throw { message: "Error: FoodController.removeCategory", done }
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
}

module.exports = {
  foodController: new FoodController()
};
