const { adminController } = require('./admin.controller');
const { pcfController } = require('./pcf.controller');
const { foodController } = require('./food.controller');
const { restaurantController } = require('./restaurant.controller');
const { orderController } = require('./order.controller');
const { superAdminController } = require('./superadmin.controller')
const { websiteController } = require('./website.controller')



module.exports = {
  adminController,
  pcfController,
  foodController,
  restaurantController,
  orderController,
  websiteController,
  superAdminController
};