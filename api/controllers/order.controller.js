const { Order, AdminUser, Notifications, Restaurant, WebCommission, DriverPayment, DriverOrders, InHouse, MyIncome, Cart, DriverIncome, DriverCharges } = require('../models');
const moment = require("moment");
const { Driver, PushNotification } = require('../models/user.model');
const firebase = require("./../../config/firebase")
const axios = require('axios').default;

var FCM = require('fcm-node');
var ownerServerKey = 'AAAA9_jNx1w:APA91bHiD8OD7ySx90E8JlGUF51R5g8IoRhajgMuT1I2jI736H8TMDf9791EVBdSXd45YlRikBmsLgvTCHApPxWhnY6oP0fgQ5oFW0QKU6Ye3tzob32zcLFTLXSydZ8zN41wThtFq_lH'; //put your server key here
var ownerFcm = new FCM(ownerServerKey);
var serverKey = 'AAAA4eT6iKU:APA91bGnW-pS7akarc56ctfTg-918Q-7PLZ0aH5bjqSLH5PxzWmirWskFoBYhVIdbxz4Qet8te_DIPtUiou9s0RLnsHzoTzgAQ_f8LVEOAq3sakl6aQgmg6CfdvKNUDn6MLa60io_zP7'; //put your server key here
var fcm = new FCM(serverKey);

class OrderController {
	async fetchOrder(req, res) {
		try {
			const data = await Order.findById(req.params.id)
				.populate('user')
				.populate('driver')
				.populate('restaurant')
				.exec();
			if (!data) {
				throw new Error('No Order Found');
			}
			res.json({ data });
		} catch (error) {
			res.json({
				error: error.message
			});
		}
	}
	async fetchDriverOrder(req, res) {
		try {
			const data = await DriverOrders.findById(req.params.id)
				.populate('user')
				.populate('driver')
				.populate('restaurant')
				.exec();
			if (!data) {
				throw new Error('No Order Found');
			}
			res.json({ data });
		} catch (error) {
			res.json({
				error: error.message
			});
		}
	}
	async fetchOrderByRestaurant(req, res) {
		try {
			const data = await Order.find({ restaurant: req.params.id })
				.populate('user')
				.populate('driver')
				.populate('foodItems')
				.exec();
			if (!data) {
				throw new Error('No Order Found');
			}
			res.json({ data });
		} catch (error) {
			res.json({
				error: error.message
			});
		}
	}
	async fetchOrderByDriver(req, res) {
		try {
			const orders = await DriverOrders.find({ driver: req.params.id })
				.populate('user')
				.populate('foodItems')
				.populate('restaurant')
				.exec();
			if (!orders) {
				throw new Error('No Order Found');
			}
			let data = []
			orders.forEach(element => {
				if (element.orderStatus !== "recieved") {
					data.push(element)
				}
			});
			res.json({ data });
		} catch (error) {
			res.json({
				error: error.message
			});
		}
	}
	async fetchDriverOrders(req, res) {
		try {
			const data = await DriverOrders.find({ driver: req.params.id })
				.populate('user')
				.populate('foodItems')
				.populate('restaurant')
				.exec();
			if (!data) {
				throw new Error('No Order Found');
			}
			res.json(data);
		} catch (error) {
			res.json({
				error: error.message
			});
		}
	}
	async fetchOrderByDriverFromRestaurants(req, res) {
		try {
			const user = await Driver.find({ _id: req.params.id })
				.populate('restaurants')
				.exec();
			let data = []
			for (let i = 0; i < user[0].restaurants.length; i++) {
				const id = user[0].restaurants[i]
				const order = await Order.find({ restaurant: id })
					.populate('user')
					.populate('driver')
					.populate('restaurant')
					.populate('foodItems')
					.exec();
				if (order.length) {
					data.push(order)
				}
			}
			if (!data[0]) {
				throw new Error('No Order Found');
			}
			return res.json(data[0]);
		} catch (error) {
			res.json({
				error: error.message
			});
		}
	}
	async fetchPendingOrderByDriverFromRestaurants(req, res) {
		try {
			const user = await Driver.find({ _id: req.params.id })
				.populate('restaurants')
				.exec();
			let data = []
			for (let i = 0; i < user[0].restaurants.length; i++) {
				const id = user[0].restaurants[i]
				const order = await Order.find({ restaurant: id, orderStatus: "recieved" })
					.populate('user')
					.populate('driver')
					.populate('restaurant')
					.populate('foodItems')
					.exec();
				if (order.length) {
					data.push(order)
				}
			}
			if (!data[0]) {
				throw new Error('No Order Found');
			}
			return res.json(data[0]);
		} catch (error) {
			res.json({
				error: error.message
			});
		}
	}
	async fetchDriverEarning(req, res) {
		try {
			const data = await Order.find({ driver: req.params.id })
				.select('orderNumber address payment')
				.exec();
			const num = data
				.map(item => +item.payment)
			const sum = num.reduce(function (a, b) {
				return a + b;
			}, 0);
			if (!data) {
				throw new Error('No Order Found');
			}
			const earning = {
				data,
				sum
			}
			res.json({ earning });
		} catch (error) {
			res.json({
				error: error.message
			});
		}
	}
	async fetchOrderByUser(req, res) {
		try {
			const data = await Order.find({ user: req.params.id })
				.populate('driver')
				.populate('foodItems')
				.populate('restaurant')
				.exec();
			if (!data) {
				throw new Error('No Order Found');
			}
			res.json({ data: data.reverse() });
		} catch (error) {
			res.json({
				error: error.message
			});
		}
	}
	async fetchDriverPendingOrders(req, res) {
		try {
			const data = await DriverOrders.find({ driver: req.params.id, orderStatus: "recieved" }).exec();
			if (!data) {
				res.json({ error: "No Pending Orders Found!" });
			}
			res.json({ data });
		} catch (error) {
			res.json({
				error: error.message
			});
		}
	}
	async fetchInHouseOrderByDrive(req, res) {
		try {
			const data = await InHouse.find({ driver: req.params.id })
				.populate('driver')
				.populate('restaurant')
				.exec();
			if (!data) {
				throw new Error('No Order Found');
			}
			res.json({ data });
		} catch (error) {
			res.json({
				error: error.message
			});
		}
	}
	async fetchInHouseOrderByOwner(req, res) {
		try {
			const data = await InHouse.find({ restaurant: req.params.id })
				.populate('driver')
				.populate('restaurant')
				.exec();
			if (!data) {
				throw new Error('No Order Found');
			}
			res.json({ data: data.reverse() });
		} catch (error) {
			res.json({
				error: error.message
			});
		}
	}
	async fetchOrderByOnwer(req, res) {
		try {
			const user = await AdminUser.find({ _id: req.params.id })
				.populate('restaurants')
				.exec();
			let data = []
			for (let i = 0; i < user[0].restaurants.length; i++) {
				const id = user[0].restaurants[i]
				const order = await Order.find({ restaurant: id })
					.populate('user')
					.populate('driver')
					.populate('restaurant')
					.populate('foodItems')
					.exec();
				if (order.length) {
					data.push(order)
				}
			}
			if (!data[0]) {
				throw new Error('No Order Found');
			}
			return res.json(data[0].reverse());
		} catch (error) {
			res.json({
				error: error.message
			});
		}
	}
	async updateDriverServicesOff(req, res) {
		try {
			const orders = await Order.find({ riderServices: false }).exec();
			orders.forEach(async element => {
				const pickUpTime = moment(element.recievedTime, 'h:mm A')
					.add(element.restaurant.pickupTime, 'minutes')
					.format('h:mm A');
				var a = moment(new Date);
				var b = moment(element.createdAt);
				const diff = a.diff(b, 'minutes')
				if (diff > element.restaurant.pickupTime) {
					const updateData = {
						orderStatus: "delivered"
					}
					const updateOrder = await Order.findByIdAndUpdate(element._id, updateData, {
						new: true
					})
						.exec();
				}
			});

			return res.json({ data: "status changed" });
		} catch (error) {
			res.json({
				error: error.message
			});
		}
	}
	async fetchOrderByOnwerType(req, res) {
		try {
			const user = await AdminUser.find({ _id: req.params.id })
				.populate('restaurants')
				.exec();
			let data = []
			for (let i = 0; i < user[0].restaurants.length; i++) {
				const type = req.query.type;
				const id = user[0].restaurants[i]
				const order = await Order.find({ restaurant: id, paymentMethod: type })
					.populate('user')
					.populate('driver')
					.populate('restaurant')
					.populate('foodItems')
					.exec();
				if (order.length) {
					data.push(order)
				}
			}
			if (!data[0]) {
				throw new Error('No Order Found');
			}
			return res.json(data[0]);
		} catch (error) {
			res.json({
				error: error.message
			});
		}
	}
	async fetchPendingOrderByOnwer(req, res) {
		try {
			const user = await AdminUser.find({ _id: req.params.id })
				.populate('restaurants')
				.exec();
			let data = []
			for (let i = 0; i < user[0].restaurants.length; i++) {
				const id = user[0].restaurants[i]
				const order = await Order.find({ restaurant: id, orderStatus: "pending" })
					.populate('user')
					.populate('driver')
					.populate('restaurant')
					.populate('foodItems')
					.exec();
				if (order.length) {
					data.push(order)
				}
			}
			if (!data[0]) {
				throw new Error('No Order Found');
			}
			return res.json(data[0].reverse());
		} catch (error) {
			res.json({
				error: error.message
			});
		}
	}
	async fetchCollection(req, res) {
		try {
			const data = await Order.find({ orderStatus: "recieved", orderType: "pickup" })
				.populate("restaurant")
				.exec();

			return res.json({ data });
		} catch (error) {
			res.json({
				error: error.message
			});
		}
	}
	async fetchAllPendingOrders(req, res) {
		try {
			const orders = await Order.find({ orderStatus: "pending" })
				.exec();

			return res.json({ data: orders });
		} catch (error) {
			res.json({
				error: error.message
			});
		}
	}
	async fetchRecievedOrderByOnwer(req, res) {
		try {
			const user = await AdminUser.find({ _id: req.params.id })
				.populate('restaurants')
				.exec();
			let data = []
			for (let i = 0; i < user[0].restaurants.length; i++) {
				const id = user[0].restaurants[i]
				const order = await Order.find({ restaurant: id, orderStatus: "recieved" })
					.populate('user')
					.populate('driver')
					.populate('restaurant')
					.populate('foodItems')
					.exec();
				if (order.length) {
					data.push(order)
				}
			}
			if (!data[0]) {
				throw new Error('No Order Found');
			}
			return res.json(data[0]);
		} catch (error) {
			res.json({
				error: error.message
			});
		}
	}
	async fetchOrderByOnwerDate(req, res) {
		try {
			const user = await AdminUser.find({ _id: req.params.id })
				.populate('restaurants')
				.exec();
			let data = []
			for (let i = 0; i < user[0].restaurants.length; i++) {
				const id = user[0].restaurants[i]
				const order = await Order.find({ restaurant: id })
					.populate('user')
					.populate('driver')
					.populate('restaurant')
					.populate('foodItems')
					.exec();
				const date = req.query.date
				order.forEach(element => {
					if (date === "week") {
						var lastWeekDate = moment(new Date, "DD/MM/YYYY").subtract(7, 'd');
						const createdDate = moment(element.createdAt, "DD/MM/YYYY").isAfter(lastWeekDate)
						if (createdDate) {
							data.push(element)
						}
					} else if (date === "month") {
						var lastWeekDate = moment(new Date, "DD/MM/YYYY").subtract(30, 'd');
						const createdDate = moment(element.createdAt, "DD/MM/YYYY").isAfter(lastWeekDate)
						if (createdDate) {
							data.push(element)
						}
					}
				});

				// if (order.length) {
				// 	data.push(order)
				// }
			}
			if (!data[0]) {
				throw new Error('No Order Found');
			}
			return res.json(data);
		} catch (error) {
			res.json({
				error: error.message
			});
		}
	}
	async fetchAllOrder(req, res) {
		try {
			const data = await Order.find().exec();
			if (!data) {
				throw new Error('No Order in db');
			}
			res.json({ data });
		} catch (error) {
			res.json({
				error: error.message
			});
		}
	}

	async createOrder(req, res) {
		try {
			const allRrestaurants = await Restaurant.find({ _id: req.body.restaurant }).exec();
			const allOrders = await Order.find().exec();
			const ordersLength = allOrders.length
			let orderNumber = ""
			if (ordersLength > 99) {
				orderNumber = allRrestaurants[0].name.substring(0, 3).toUpperCase() + ordersLength
			} else if (ordersLength > 9) {
				orderNumber = allRrestaurants[0].name.substring(0, 3).toUpperCase() + "0" + ordersLength
			} else {
				orderNumber = allRrestaurants[0].name.substring(0, 3).toUpperCase() + "00" + ordersLength
			}

			const cartData = await Cart.find({ user: req.body.user }).exec()

			cartData.forEach(async element => {
				const shoppingCart = await Cart.deleteOne({ _id: element._id }).exec()
			});

			const orderData = {
				address: req.body.address,
				customerName: req.body.customerName,
				deliveryCharge: req.body.deliveryCharge,
				deliveryTime: req.body.deliveryTime,
				discount: req.body.discount,
				orderNumber: orderNumber,
				orderStatus: req.body.orderStatus,
				paymentMethod: req.body.paymentMethod,
				phoneNumber: req.body.phoneNumber,
				distance: req.body.distance,
				discount: req.body.discount,
				user: req.body.user,
				type: req.body.type,
				orderType: req.body.orderType,
				totalPrice: req.body.totalPrice,
				kitchenNotes: req.body.kitchenNotes,
				restaurant: req.body.restaurant,
				driver: req.body.driver,
				foodItems: req.body.foodItems,
				riderServices: allRrestaurants[0].riderServices
			}


			const order = new Order(orderData);
			await order.save();
			if (!order) {
				return res.status(400).json({ msg: 'order didnt added' });
			}

			let totalPrice = 0
			req.body.foodItems.forEach(foods => {
				totalPrice = totalPrice + foods.price
			});
			const restaurant = await Restaurant.find({ _id: req.body.restaurant }).exec();
			let ammount = Math.ceil(parseInt(restaurant[0].commission) / 100 * totalPrice)
			const commissionData = {
				ammount,
				userId: req.body.restaurant,
				userType: 'restaurant',
				commissionData: JSON.stringify(order)
			}

			const commission = new WebCommission(commissionData);
			await commission.save();
			if (!commission) {
				return res.status(400).json({ msg: 'Commission was not added' });
			}

			const ownerId = await AdminUser.find({ 'restaurants': order.restaurant })
				.select('_id').exec();
			const itemsInString = order.foodItems.map(item => item.title).toString();
			let ownernotificationBody = {
				title: req.body.customerName,
				description: `ordered to ${restaurant[0].name}`,
				type: 'owner',
				userId: ownerId[0]._id,
				notificationFor: "order",
				data: JSON.stringify(order)
			}
			const pushNotification = await PushNotification.findOne({ userId: ownerId[0]._id }).exec();

			if (pushNotification) {
				var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
					to: pushNotification.fcm_token,
					// collapse_key: 'your_collapse_key',

					notification: {
						title: req.body.customerName,
						body: `ordered to ${restaurant[0].name}`,
						android_channel_id: 'partner_app_channel',
						sound: 'sound',
					},

					data: {  //you can send only notification or only data(or include both)
						notificationFor: "order",
						data_string: JSON.stringify(order)
					}
				};

				ownerFcm.send(message, function (err, response) {
					if (err) {
						console.log({ error: "Something has gone wrong!" });
					} else {
						console.log({ response });
					}
				});
			}


			const ownerNotifi = firebase.database().ref(`${ownerId[0]._id}`);
			const ownerNotifiId = ownerNotifi.push(ownernotificationBody).getKey(); // add notification and get its id
			const ownerNotifiItems = firebase.database().ref(`${ownerId[0]._id}`);
			ownerNotifiItems.on('value', (snapshot) => {
				let items = snapshot.val();
				let newState = [];
				for (let item in items) {
					newState.push({
						id: item,
						title: items[item].title,
						userId: items[item].userId
					});
				}
				setTimeout(
					() => newState.map((item) => {
						if (item.id === ownerNotifiId) {
							const removeItems = firebase.database().ref(`/${ownerId[0]._id}/${item.id}`);
							removeItems.remove();
						}
					}),
					60000
				);
			});

			const ownerNotification = new Notifications(ownernotificationBody);
			await ownerNotification.save();
			if (!ownerNotification) {
				return res.status(400).json({ msg: 'Notification not sent!' });
			}
			let usernotificationBody = {
				title: `Your`,
				description: `order is placed in ${restaurant[0].name}.`,
				type: 'user',
				userId: order.user,
				notificationFor: "order",
				data: JSON.stringify(order)
			}
			const itemsRef = firebase.database().ref(`${order.user}`);
			const newId = itemsRef.push(usernotificationBody).getKey(); // add notification and get its id
			const getItems = firebase.database().ref(`${order.user}`);
			getItems.on('value', (snapshot) => {
				let items = snapshot.val();
				let newState = [];
				for (let item in items) {
					newState.push({
						id: item,
						title: items[item].title,
						userId: items[item].userId
					});
				}
				setTimeout(
					() => newState.map((item) => {
						if (item.id === newId) {
							const removeItems = firebase.database().ref(`/${order.user}/${item.id}`);
							removeItems.remove();
						}
					}),
					10000
				);
			});
			const userNotification = new Notifications(usernotificationBody);
			await userNotification.save();
			if (!userNotification) {
				return res.status(400).json({ msg: 'Notification not sent!' });
			}
			res.json({ data: order, notification: ownerNotification, commission });
		} catch (error) {
			res.status(500).send(error.message);
		}
	}

	async createAcceptOrder(req, res) {
		try {
			const updateData = {
				orderStatus: "Assigned to rider",
				driver: req.body.driverId
			}
			const order = await Order.findByIdAndUpdate(req.body.orderId, updateData, {
				new: true
			})
				.exec();

			const driverOrder = await DriverOrders.find({ orderId: req.body.orderId, orderStatus: "Assigned to rider" }).exec();
			if (driverOrder.length) {
				res.json({ error: "This order is already taken by a driver" });
			} else {
				const updateDriverOrder = await DriverOrders.findByIdAndUpdate(req.body.id, updateData, {
					new: true
				})
					.exec();


				res.json({ data: updateDriverOrder });
			}
		} catch (error) {
			res.status(500).send(error.message);
		}
	}

	async pickUpOrder(req, res) {
		try {
			const updateData = {
				orderStatus: "on the way",
				driver: req.body.driverId,
				pickupTime: moment(new Date).format("hh:mm A")
			}
			const id = req.body.orderId
			const order = await Order.findByIdAndUpdate(id, updateData, {
				new: true
			})
				.exec();
			if (order && order.user) {
				const devices = await PushNotification.find({ userId: order.user }).exec();
				if (devices) {
					devices.forEach(element => {
						var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
							to: element.fcm_token,
							// collapse_key: 'your_collapse_key',

							notification: {
								title: "Order",
								body: "Your order is on the way!"
							},

							data: {  //you can send only notification or only data(or include both)
								notificationFor: "order",
								data_string: JSON.stringify(order),
							}
						};

						fcm.send(message, function (err, response) {
							if (err) {
								console.log({ error: "Something has gone wrong with push notification!" });
							} else {
								console.log({ response });
							}
						});
					});
				}
			}


			const updateDriverOrder = await DriverOrders.findByIdAndUpdate(req.body.id, updateData, {
				new: true
			})
				.exec();
			res.json({ data: updateDriverOrder });
		} catch (error) {
			res.status(500).send(error.message);
		}
	}

	async deliverOrder(req, res) {
		try {
			const updateData = {
				orderStatus: "delivered",
				driver: req.body.driverId,
				deliveredAt: moment(new Date).format("hh:mm A")
			}
			const order = await Order.findByIdAndUpdate(req.body.orderId, updateData, {
				new: true
			})
				.populate("restaurant")
				.exec();

			const driverCharges = await DriverCharges.find().exec();
			const distance = order.distance ? parseInt(order.distance) : 0
			let ammount = 0
			driverCharges.forEach(element => {
				const miles = parseInt(element.miles)
				if (distance >= miles) {
					ammount = parseFloat(element.charges)
				}
			});
			const driverIncome = {
				userId: req.body.driverId,
				userType: "driver",
				incomeData: JSON.stringify(order),
				incomeType: "order",
				totalIncome: ammount
			}

			const thisDriverIncome = new MyIncome(driverIncome);
			await thisDriverIncome.save()

			const owners = await AdminUser.find()
				.exec();
			let ownerId = []
			owners.forEach(element => {
				element.restaurants.forEach(rest => {
					if (JSON.stringify(rest) === JSON.stringify(order.restaurant._id)) {
						ownerId.push(element._id)
					}
				});
			});
			const ownerIncome = {
				userId: ownerId[0],
				userType: "owner",
				incomeData: JSON.stringify(order),
				incomeType: "order",
				totalIncome: order.totalPrice,
			}
			const myIncome = new MyIncome(ownerIncome);
			await myIncome.save()

			const devices = await PushNotification.find({ userId: ownerId[0] }).exec();
			if (devices) {
				devices.forEach(element => {
					var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
						to: element.fcm_token,
						// collapse_key: 'your_collapse_key',

						notification: {
							title: `Order # ${order.orderNumber}`,
							body: `Order # ${order.orderNumber} is delivered!`
						},

						data: {  //you can send only notification or only data(or include both)
							notificationFor: "order",
							data_string: JSON.stringify(order),
						}
					};

					ownerFcm.send(message, function (err, response) {
						if (err) {
							console.log({ error: "Something has gone wrong with push notification!" });
						} else {
							console.log({ response });
						}
					});
				});
			}

			const updateDriverOrder = await DriverOrders.findByIdAndUpdate(req.body.id, updateData, {
				new: true
			})
				.exec();


			const driverIncomeData = {
				driver: req.body.driverId,
				restaurant: updateDriverOrder.restaurant,
				ammount
			}

			const incomeDriver = new DriverIncome(driverIncomeData);
			await incomeDriver.save();
			if (!incomeDriver) {
				return res.status(400).json({ msg: 'driver income did not saved' });
			}


			res.json({ data: updateDriverOrder });
		} catch (error) {
			res.status(500).send(error.message);
		}
	}

	async deliverInHouseOrder(req, res) {
		try {
			const updateData = {
				orderStatus: "delivered",
				driver: req.body.driverId,
				deliveredAt: moment(new Date).format("hh:mm A")
			}
			const updateDriverOrder = await DriverOrders.findByIdAndUpdate(req.body.id, updateData, {
				new: true
			})
				.populate("restaurant")
				.exec();


			const driverCharges = await DriverCharges.find().exec();
			const distance = updateDriverOrder.distance ? parseInt(updateDriverOrder.distance) : 0
			let ammount = 0
			driverCharges.forEach(element => {
				const miles = parseInt(element.miles)
				if (distance >= miles) {
					ammount = parseFloat(element.charges)
				}
			});
			const driverIncome = {
				userId: req.body.driverId,
				userType: "driver",
				incomeData: JSON.stringify(updateDriverOrder),
				incomeType: "order",
				totalIncome: ammount
			}

			const thisDriverIncome = new MyIncome(driverIncome);
			await thisDriverIncome.save()


			const driverIncomeData = {
				driver: req.body.driverId,
				restaurant: updateDriverOrder.restaurant,
				ammount
			}

			const incomeDriver = new DriverIncome(driverIncomeData);
			await incomeDriver.save();
			if (!incomeDriver) {
				return res.status(400).json({ msg: 'driver income did not saved' });
			}


			const owners = await AdminUser.find()
				.exec();
			let ownerId = []
			owners.forEach(element => {
				element.restaurants.forEach(rest => {
					if (JSON.stringify(rest) === JSON.stringify(updateDriverOrder.restaurant._id)) {
						ownerId.push(element._id)
					}
				});
			});

			const devices = await PushNotification.find({ userId: ownerId[0] }).exec();
			if (devices) {
				devices.forEach(element => {
					var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
						to: element.fcm_token,
						// collapse_key: 'your_collapse_key',

						notification: {
							title: `Order Delivery`,
							body: `Order # ${updateDriverOrder.orderNumber} is delivered!`
						},

						data: {  //you can send only notification or only data(or include both)
							notificationFor: "order",
							data_string: JSON.stringify(updateDriverOrder),
						}
					};

					ownerFcm.send(message, function (err, response) {
						if (err) {
							console.log({ error: "Something has gone wrong with push notification!" });
						} else {
							console.log({ response });
						}
					});
				});
			}


			res.json({ data: updateDriverOrder });
		} catch (error) {
			res.status(500).send(error.message);
		}
	}

	async collectOrder(req, res) {
		try {
			const updateData = {
				orderStatus: "delivered",
				deliveredAt: moment(new Date).format("hh:mm A"),
			}
			const order = await Order.findByIdAndUpdate(req.body.orderId, updateData, {
				new: true
			})
				.populate("restaurant")
				.exec();

			const owners = await AdminUser.find()
				.exec();
			let ownerId = []
			owners.forEach(element => {
				element.restaurants.forEach(rest => {
					if (JSON.stringify(rest) === JSON.stringify(order.restaurant._id)) {
						ownerId.push(element._id)
					}
				});
			});
			const ownerIncome = {
				userId: ownerId[0],
				userType: "owner",
				incomeData: JSON.stringify(order),
				incomeType: "order",
				totalIncome: order.totalPrice,
			}

			const myIncome = new MyIncome(ownerIncome);
			await myIncome.save()

			const devices = await PushNotification.find({ userId: ownerId[0] }).exec();
			if (devices) {
				devices.forEach(element => {
					var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
						to: element.fcm_token,
						// collapse_key: 'your_collapse_key',

						notification: {
							title: `Order # ${order.orderNumber}`,
							body: `Order # ${order.orderNumber} is collected!`
						},

						data: {  //you can send only notification or only data(or include both)
							notificationFor: "order",
							data_string: JSON.stringify(order),
						}
					};

					ownerFcm.send(message, function (err, response) {
						if (err) {
							console.log({ error: "Something has gone wrong with push notification!" });
						} else {
							console.log({ response });
						}
					});
				});
			}

			res.json({ result: "Order Collected" });
		} catch (error) {
			res.status(500).send(error.message);
		}
	}

	async createAcceptOrderByOwner(req, res) {
		try {
			const updateData = {
				orderStatus: "recieved",
				recievedTime: moment(new Date).format("hh:mm A")
			}
			const orderUpdate = await Order.findByIdAndUpdate(req.body.orderId, updateData, {
				new: true
			})
				.populate("restaurant")
				.populate("user")
				.exec();

			const myRestaurant = await Restaurant.find({ _id: orderUpdate.restaurant }).exec()
			let usernotificationBody = {
				title: `Your`,
				description: `order is accepted by ${myRestaurant[0].name}.`,
				type: 'user',
				userId: orderUpdate.user._id,
				notifictioFor: "order",
				data: JSON.stringify(orderUpdate)
			}

			const userItemsRef = firebase.database().ref(`${orderUpdate.user._id}`);
			const userNewId = userItemsRef.push(usernotificationBody).getKey(); // add notification and get its id
			const userGetItems = firebase.database().ref(`${orderUpdate.user._id}`);
			userGetItems.on('value', (snapshot) => {
				let items = snapshot.val();
				let newState = [];
				for (let item in items) {
					newState.push({
						id: item,
						title: items[item].title,
						userId: items[item].userId
					});
				}
				setTimeout(
					() => newState.map((item) => {
						if (item.id === userNewId) {
							const removeItems = firebase.database().ref(`/${orderUpdate.user._id}/${item.id}`);
							removeItems.remove();
						}
					}),
					10000
				);
			});

			const userNotification = new Notifications(usernotificationBody);
			await userNotification.save();
			if (!userNotification) {
				console.log({ error: "Notification was not saved" })
			}

			const userDevices = await PushNotification.find({ userId: orderUpdate.user._id }).exec();
			if (userDevices.length) {
				userDevices.forEach(device => {
					var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
						to: device.fcm_token,
						// collapse_key: 'your_collapse_key',

						notification: {
							title: `Order # ${orderUpdate.orderNumber}`,
							body: `Your order # ${orderUpdate.orderNumber} is accepted by ${orderUpdate.restaurant.name}`
						},

						data: {  //you can send only notification or only data(or include both)
							notificationFor: "order",
							// notificationFor: "booking",
							data_string: JSON.stringify(orderUpdate),
						}
					};

					fcm.send(message, function (err, response) {
						if (err) {
							console.log({ error: "Something has gone wrong with push notification!" });
						} else {
							console.log({ response });
						}
					});
				});
			}

			if (orderUpdate.orderType === "delivery" && orderUpdate.restaurant.riderServices) {
				const id = orderUpdate.restaurant._id
				const restaurant = await Restaurant.find({ _id: id }).exec();
				const postCodes = await axios.get(`https://api.postcodes.io/postcodes/${restaurant[0].postCode}/nearest?radius=2000`);
				let postCodeData = postCodes.data.result
				let drivers = []
				for (let i = 0; i < postCodeData.length; i++) {
					const element = postCodeData[i];
					// const code = element.postcode.replace(/\s/g, '')
					const code = element.postcode
					const driver = await Driver.find({ workPlace: code, isAvailable: true })
					if (driver.length) {
						driver.forEach(element => {
							drivers.push(element)
						});
					}
				}
				drivers.forEach(async item => {
					const order = await DriverOrders.find({ orderId: req.body.orderId }).exec();
					if (order.length) {
						res.json({ error: "This order is already taken by a driver" });
					}

					const driverDevices = await PushNotification.find({ userId: item._id }).exec();
					if (driverDevices.length) {
						driverDevices.forEach(device => {
							var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
								to: device.fcm_token,
								// collapse_key: 'your_collapse_key',

								notification: {
									title: `New Order`,
									body: `There is an new order in ${orderUpdate.restaurant.name}`,
									android_channel_id: "123",
								},

								data: {  //you can send only notification or only data(or include both)
									notificationFor: "order",
									data_string: JSON.stringify(orderUpdate),
								}
							};

							fcm.send(message, function (err, response) {
								if (err) {
									console.log({ error: "Something has gone wrong with push notification!" });
								} else {
									console.log({ response });
								}
							});
						});
					}

					const driverOrder = {
						...orderUpdate,
						orderStatus: "recieved",
						driver: item._id,
						orderNumber: orderUpdate.orderNumber,
						orderId: req.body.orderId,
						customerName: orderUpdate.user.name,
						address: orderUpdate.address,
						distance: orderUpdate.distance,
						deliveryTime: orderUpdate.deliveryTime,
						phoneNumber: orderUpdate.phoneNumber,
						deliveryCharge: orderUpdate.deliveryCharge,
						paymentMethod: orderUpdate.paymentMethod,
						user: orderUpdate.user._id,
						restaurant: orderUpdate.restaurant,
						totalPrice: orderUpdate.totalPrice
					}
					const newDriverOrder = new DriverOrders(driverOrder);
					await newDriverOrder.save()
					const notificationData = {
						notification: {
							title: `New`,
							description: `order placed at ${orderUpdate.restaurant.name}.`,
							type: 'driver',
							userId: item._id,
							notificationFor: "order",
							data: JSON.stringify(orderUpdate)
						},
						userId: item._id
					}
					const body = notificationData
					const itemsRef = firebase.database().ref(`${body.userId}`);
					const newId = itemsRef.push(body.notification).getKey(); // add notification and get its id
					const getItems = firebase.database().ref(`${body.userId}`);
					getItems.on('value', (snapshot) => {
						let items = snapshot.val();
						let newState = [];
						for (let item in items) {
							newState.push({
								id: item,
								title: items[item].title,
								userId: items[item].userId
							});
						}
						setTimeout(
							() => newState.map((item) => {
								if (item.id === newId) {
									const removeItems = firebase.database().ref(`/${body.userId}/${item.id}`);
									removeItems.remove();
								}
							}),
							10000
						);
					});
					const notification = new Notifications(body);
					await notification.save();
					if (!notification) {
						return res.status(400).json({ msg: 'Notification not sent!' });
					}
					const ownerDevices = await PushNotification.findOne({ userId: item.id }).exec();
					if (ownerDevices) {
						var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
							to: ownerDevices.fcm_token,
							// collapse_key: 'your_collapse_key',

							notification: {
								title: "New Order",
								body: `${orderUpdate.restaurant.name} got an order`
							},

							data: {  //you can send only notification or only data(or include both)
								notificationFor: "order",
								data_string: JSON.stringify(orderUpdate),
								// actions: [
								// 	{
								// 		inline: true,
								// 		callback: "accept",
								// 		foreground: false,
								// 		title: "Accept"
								// 	},
								// 	{
								// 		icon: "snooze",
								// 		callback: "reject",
								// 		foreground: false,
								// 		title: "Reject"
								// 	}
								// ]
							}
						};

						fcm.send(message, function (err, response) {
							if (err) {
								console.log({ error: "Something has gone wrong with push notification!" });
							} else {
								console.log({ response });
							}
						});
					}
				});
			}
			res.json({ data: "Order accepted successfuly" });
		} catch (error) {
			res.status(500).send(error.message);
		}
	}

	async createInHouseOrder(req, res) {
		try {
			const allRrestaurants = await Restaurant.find({ _id: req.body.restaurant }).exec();
			const allOrders = await InHouse.find().exec();
			const ordersLength = allOrders.length
			let orderNumber = ""
			if (ordersLength) {
				if (ordersLength > 99) {
					orderNumber = allRrestaurants[0].name.substring(0, 3).toUpperCase() + ordersLength
				} else if (ordersLength > 9) {
					orderNumber = allRrestaurants[0].name.substring(0, 3).toUpperCase() + "0" + ordersLength
				} else {
					orderNumber = allRrestaurants[0].name.substring(0, 3).toUpperCase() + "00" + ordersLength
				}
			} else {
				orderNumber = allRrestaurants[0].name.substring(0, 3).toUpperCase() + ordersLength
			}

			const reqRestaurant = await Restaurant.findById(req.body.restaurant)
				.exec();

			const postCode1 = req.body.postCode;
			const postCode2 = reqRestaurant.postCode;
			const userPostCodeData = await axios.get(`https://api.postcodes.io/postcodes/${postCode1}`);
			let userlatitude = userPostCodeData.data.result.latitude
			let userlongitude = userPostCodeData.data.result.longitude
			const restaurantPostCodeData = await axios.get(`https://api.postcodes.io/postcodes/${postCode2}`);
			let restaurantlatitude = restaurantPostCodeData.data.result.latitude
			let restaurantlongitude = restaurantPostCodeData.data.result.longitude

			var R = 6371; // km

			var dLat = (restaurantlatitude - userlatitude) * Math.PI / 180
			var dLon = (restaurantlongitude - userlongitude) * Math.PI / 180
			var lat1 = userlatitude * Math.PI / 180
			var lat2 = userlongitude * Math.PI / 180

			var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
				Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
			let distance = R * c;

			const driverCharges = await DriverCharges.find().exec();
			let ammount = 0
			driverCharges.forEach(element => {
				const miles = parseInt(element.miles)
				if (distance >= miles) {
					ammount = parseFloat(element.charges)
				}
			});

			console.log({ ammount, distance })
			const orderData = {
				address: req.body.address + ", " + req.body.postCode,
				name: req.body.customerName,
				orderNumber,
				deliveryCharges: ammount,
				deliveryTime: req.body.deliveryTime,
				orderStatus: req.body.orderStatus,
				orderType: req.body.orderType,
				paymentMethod: req.body.paymentMethod,
				phoneNumber: req.body.phoneNumber,
				distance: distance,
				user: req.body.user,
				totalPrice: req.body.totalPrice,
				restaurant: req.body.restaurant,
			}
			const order = new InHouse(orderData);
			await order.save();
			if (!order) {
				return res.status(400).json({ msg: 'order didnt added' });
			}
			const ownerId = await AdminUser.find({ 'restaurants': order.restaurant })
				.select('_id').exec();
			let ownernotificationBody = {
				title: "In-House",
				description: `order placed`,
				type: 'owner',
				userId: ownerId[0]._id,
				notificationFor: "in-house",
				data: JSON.stringify(order)
			}
			const ownerNotifi = firebase.database().ref(`${ownerId[0]._id}`);
			const ownerNotifiId = ownerNotifi.push(ownernotificationBody).getKey(); // add notification and get its id
			const ownerNotifiItems = firebase.database().ref(`${ownerId[0]._id}`);
			ownerNotifiItems.on('value', (snapshot) => {
				let items = snapshot.val();
				let newState = [];
				for (let item in items) {
					newState.push({
						id: item,
						title: items[item].title,
						userId: items[item].userId
					});
				}
				setTimeout(
					() => newState.map((item) => {
						if (item.id === ownerNotifiId) {
							const removeItems = firebase.database().ref(`/${ownerId[0]._id}/${item.id}`);
							removeItems.remove();
						}
					}),
					10000
				);
			});
			const ownerNotification = new Notifications(ownernotificationBody);
			await ownerNotification.save();
			if (!ownerNotification) {
				return res.status(400).json({ msg: 'Notification not sent!' });
			}
			const restaurant = await Restaurant.find({ _id: req.body.restaurant }).exec();
			const postCodes = await axios.get(`https://api.postcodes.io/postcodes/${restaurant[0].postCode}/nearest?radius=2000`);
			let postCodeData = postCodes.data.result
			let data = []
			for (let i = 0; i < postCodeData.length; i++) {
				const element = postCodeData[i];
				// const code = element.postcode.replace(/\s/g, '')
				const code = element.postcode
				const driver = await Driver.find({ workPlace: code }).exec()
				if (driver.length) {
					driver.forEach(async element => {
						data.push(element)
					});
				}
			}
			data.forEach(async element => {
				const driverOrder = {
					...order,
					orderStatus: "recieved",
					driver: element._id,
					orderNumber: order.orderNumber,
					orderType: order.orderType,
					orderId: order._id,
					address: order.address,
					deliveryTime: order.deliveryTime,
					phoneNumber: order.phoneNumber,
					deliveryCharge: order.deliveryCharges,
					paymentMethod: order.paymentMethod,
					user: order.user,
					restaurant: order.restaurant,
					totalPrice: order.totalPrice
				}
				const newDriverOrder = new DriverOrders(driverOrder);
				await newDriverOrder.save()
				let drivernotificationBody = {
					title: `Order`,
					description: `placed at ${restaurant[0].name}.`,
					type: 'driver',
					driverId: element._id,
					notificationFor: "in-house",
					data: JSON.stringify(order)
				}
				const itemsRef = firebase.database().ref(`${element._id}`);
				const newId = itemsRef.push(drivernotificationBody).getKey(); // add notification and get its id
				const getItems = firebase.database().ref(`${element._id}`);
				getItems.on('value', (snapshot) => {
					let items = snapshot.val();
					let newState = [];
					for (let item in items) {
						newState.push({
							id: item,
							title: items[item].title,
							userId: items[item].userId
						});
					}
					setTimeout(
						() => newState.map((item) => {
							if (item.id === newId) {
								const removeItems = firebase.database().ref(`/${element._id}/${item.id}`);
								removeItems.remove();
							}
						}),
						10000
					);
				});
				const driverNotification = new Notifications(drivernotificationBody);
				await driverNotification.save();
				if (!driverNotification) {
					return res.status(400).json({ msg: 'Notification not sent!' });
				}
			});
			res.json({ data: order });
		} catch (error) {
			res.status(500).send(error.message);
		}
	}

	async updateOrder(req, res) {
		try {
			const orderUpdate = await Order.findByIdAndUpdate(req.params.id, req.body, {
				new: true
			})
				.populate("restaurant")
				.exec();
			const restaurant = await Restaurant.find({ _id: orderUpdate.restaurant }).exec()
			if (req.body.orderStatus === "recieved") {
				let usernotificationBody = {
					title: `Your`,
					description: `order is accepted by ${restaurant[0].name}.`,
					type: 'user',
					userId: orderUpdate.user,
					notifictioFor: "order",
					data: JSON.stringify(orderUpdate)
				}
				const itemsRef = firebase.database().ref(`${orderUpdate.user}`);
				const newId = itemsRef.push(usernotificationBody).getKey(); // add notification and get its id
				const getItems = firebase.database().ref(`${orderUpdate.user}`);
				getItems.on('value', (snapshot) => {
					let items = snapshot.val();
					let newState = [];
					for (let item in items) {
						newState.push({
							id: item,
							title: items[item].title,
							userId: items[item].userId
						});
					}
					setTimeout(
						() => newState.map((item) => {
							if (item.id === newId) {
								const removeItems = firebase.database().ref(`/${orderUpdate.user}/${item.id}`);
								removeItems.remove();
							}
						}),
						10000
					);
				});

				const userNotification = new Notifications(usernotificationBody);
				await userNotification.save();
				if (!userNotification) {
					return res.status(400).json({ msg: 'Notification not sent!' });
				}

				let driverNotificationBody = {
					title: `New`,
					description: `order is placed in ${restaurant[0].name}.`,
					type: 'driver',
					restaurantId: orderUpdate.restaurant,
					notifictioFor: "order",
					data: JSON.stringify(orderUpdate)
				}
				const driverNotification = new Notifications(driverNotificationBody);
				await driverNotification.save();
				if (!driverNotification) {
					return res.status(400).json({ msg: 'Notification not sent!' });
				}
			} else if (req.body.orderStatus === "declined") {
				let usernotificationBody = {
					title: `Your`,
					description: `order is declined by  ${restaurant[0].name}.Kindly re-order`,
					type: 'user',
					userId: orderUpdate.user,
					notifictioFor: "order",
					data: JSON.stringify(orderUpdate)
				}
				const declineItemsRef = firebase.database().ref(`${orderUpdate.user}`);
				const declineNewId = declineItemsRef.push(usernotificationBody).getKey(); // add notification and get its id
				const declineGetItems = firebase.database().ref(`${orderUpdate.user}`);
				declineGetItems.on('value', (snapshot) => {
					let items = snapshot.val();
					let newState = [];
					for (let item in items) {
						newState.push({
							id: item,
							title: items[item].title,
							userId: items[item].userId
						});
					}
					setTimeout(
						() => newState.map((item) => {
							if (item.id === declineNewId) {
								const removeItems = firebase.database().ref(`/${orderUpdate.user}/${item.id}`);
								removeItems.remove();
							}
						}),
						10000
					);
				});
				const userNotification = new Notifications(usernotificationBody);
				await userNotification.save();
				if (!userNotification) {
					return res.status(400).json({ msg: 'Notification not sent!' });
				}
			} else if (req.body.orderStatus === "Assigned to rider") {
				let usernotificationBody = {
					title: `Your`,
					description: `order is assigned to a rider`,
					type: 'user',
					userId: orderUpdate.user,
					notifictioFor: "order",
					data: JSON.stringify(orderUpdate)
				}
				const assignItemsRef = firebase.database().ref(`${orderUpdate.user}`);
				const assignNewId = assignItemsRef.push(usernotificationBody).getKey(); // add notification and get its id
				const assignGetItems = firebase.database().ref(`${orderUpdate.user}`);
				assignGetItems.on('value', (snapshot) => {
					let items = snapshot.val();
					let newState = [];
					for (let item in items) {
						newState.push({
							id: item,
							title: items[item].title,
							userId: items[item].userId
						});
					}
					setTimeout(
						() => newState.map((item) => {
							if (item.id === assignNewId) {
								const removeItems = firebase.database().ref(`/${orderUpdate.user}/${item.id}`);
								removeItems.remove();
							}
						}),
						10000
					);
				});
				const userNotification = new Notifications(usernotificationBody);
				await userNotification.save();
				if (!userNotification) {
					return res.status(400).json({ msg: 'Notification not sent!' });
				}
				const driverOrder = new DriverOrders(req.body);
				await driverOrder.save();
				if (!driverOrder) {
					return res.status(400).json({ msg: 'Order was not saved!' });
				}
			}
			if (!orderUpdate) {
				throw new Error('Order did not update.');
			}
			res.json({ data: orderUpdate });
		}
		catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}

	async updateInHouse(req, res) {
		try {
			if (req.body.orderStatus === "Assigned to rider") {

				const orderBody = {
					orderStatus: req.body.orderStatus,
					driver: req.body.driverId
				}
				const order = await InHouse.find({ _id: req.params.id }).exec();
				const orderUpdate = await InHouse.findByIdAndUpdate(req.params.id, orderBody, {
					new: true
				})
					.populate("restaurant")
					.exec();
				res.json({ data: orderUpdate });
			}
		}
		catch (error) {
			res.status(500).json({
				error: error.message,
			});
		}
	}

	async removeOrder(req, res) {
		try {
			const order = await await Order.findById(req.params.id).exec();
			if (order.orderStatus === "pending") {
				let usernotificationBody = {
					title: `Your`,
					description: `order is declined by ${order.restaurant}`,
					type: 'user',
					userId: order.user,
					notifictioFor: "order",
					data: JSON.stringify(order)
				}
				const assignItemsRef = firebase.database().ref(`${order.user}`);
				const assignNewId = assignItemsRef.push(usernotificationBody).getKey(); // add notification and get its id
				const assignGetItems = firebase.database().ref(`${order.user}`);
				assignGetItems.on('value', (snapshot) => {
					let items = snapshot.val();
					let newState = [];
					for (let item in items) {
						newState.push({
							id: item,
							title: items[item].title,
							userId: items[item].userId
						});
					}
					setTimeout(
						() => newState.map((item) => {
							if (item.id === assignNewId) {
								const removeItems = firebase.database().ref(`/${order.user}/${item.id}`);
								removeItems.remove();
							}
						}),
						10000
					);
				});
				const userNotification = new Notifications(usernotificationBody);
				await userNotification.save();
				if (!userNotification) {
					return res.status(400).json({ msg: 'Notification not sent!' });
				}


				const pushNotification = await PushNotification.findOne({ userId: order.user }).exec();

				if (pushNotification) {
					var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
						to: pushNotification.fcm_token,
						// collapse_key: 'your_collapse_key',

						notification: {
							title: "Notification",
							body: `Your order is declined by ${order.restaurant}`
						},

						data: {  //you can send only notification or only data(or include both)
							notificationFor: "order",
							data_string: JSON.stringify(order)
						}
					};

					fcm.send(message, function (err, response) {
						if (err) {
							console.log({ error: "Something has gone wrong!" });
						} else {
							console.log({ response });
						}
					});
				}
			}
			const done = await Order.findByIdAndRemove(req.params.id).exec();
			if (!done) {
				throw new Error("Could Not Deleted the Order")
			}
			res.json('Order Deleted')
		} catch (error) {
			res.status(500).send(error.message);
		}
	}
	async removeDriverOrder(req, res) {
		try {
			const done = await DriverOrders.findByIdAndRemove(req.params.id).exec();
			if (!done) {
				throw new Error("Could Not Deleted the Order")
			}
			res.json({ result: 'Order Deleted' })
		} catch (error) {
			res.status(500).send(error.message);
		}
	}
	async removeInHouseOrder(req, res) {
		try {
			const done = await InHouse.findByIdAndRemove(req.params.id).exec();
			if (!done) {
				throw new Error("Could Not Deleted the Order")
			}
			res.json({ result: 'Order Deleted' })
		} catch (error) {
			res.status(500).send(error.message);
		}
	}
}

module.exports = {
	orderController: new OrderController()
};
