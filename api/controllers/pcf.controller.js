const { Policy, TermsAndConditions, ReturnPolicy, Contact, FAQ } = require('../models');


class PcfController {
  // =====================> CREATE <===================== //
  async createPolicy(req, res) {
    try {
      const policy = new Policy(req.body);
      await policy.save();
      if (!policy) {
        return res.status(400).json({ msg: 'no policy added.' });
      }
      res.json({ policy });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async createTerms(req, res) {
    try {
      const terms = new TermsAndConditions(req.body);
      await terms.save();
      if (!terms) {
        return res.status(400).json({ msg: 'no terms and conditions added.' });
      }
      res.json({ terms });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async createFAQ(req, res) {
    try {
      const faq = new FAQ(req.body);
      await faq.save();
      if (!faq) {
        return res.status(400).json({ msg: 'no faq and conditions added.' });
      }
      res.json({ faq });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async createReturn(req, res) {
    try {
      const returnPolicy = new ReturnPolicy(req.body);
      await returnPolicy.save();
      if (!returnPolicy) {
        return res.status(400).json({ msg: 'no Return Policy added.' });
      }
      res.json({ returnPolicy });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async createContact(req, res) {
    try {
      const contact = new Contact(req.body);
      await contact.save();
      if (!contact) {
        return res.status(400).json({ msg: 'no Contact Us added.' });
      }
      res.json({ contact });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }

  // =====================> FETCH <===================== //
  async fetchAllPolicies(req, res) {
    try {
      const data = await Policy.find().exec();
      if (!data) {
        throw new Error('no Policies Found')
      }
      res.json(data);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async fetchAllTerms(req, res) {
    try {
      const terms = await TermsAndConditions.find().exec();
      let data = []
      terms.forEach(element => {
        if (!element.isCovid) {
          data.push(element)
        }
      });
      if (!data) {
        throw new Error('no Terms and Conditions Found')
      }
      res.json(data);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async fetchAllCovid(req, res) {
    try {
      const terms = await TermsAndConditions.find().exec();
      let data = []
      terms.forEach(element => {
        if (element.isCovid) {
          data.push(element)
        }
      });
      if (!data) {
        throw new Error('no Terms and Conditions Found')
      }
      res.json(data);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async fetchAllFaqs(req, res) {
    try {
      const data = await FAQ.find().exec();
      if (!data) {
        throw new Error('no Terms and Conditions Found')
      }
      res.json(data);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async fetchAllTermsDriver(req, res) {
    try {
      const terms = await TermsAndConditions.find({ type: "Driver" }).exec();
      let data = []
      terms.forEach(element => {
        if (!element.isCovid) {
          data.push(element)
        }
      });
      if (!data) {
        throw new Error('no Terms and Conditions Found')
      }
      res.json(data);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async fetchCovidDriver(req, res) {
    try {
      const terms = await TermsAndConditions.find({ type: "Driver" }).exec();
      let data = []
      terms.forEach(element => {
        if (element.isCovid) {
          data.push(element)
        }
      });
      if (!data) {
        throw new Error('no Terms and Conditions Found')
      }
      res.json(data);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async fetchAllFaqsDriver(req, res) {
    try {
      const data = await FAQ.find({ type: "Driver" }).exec();
      if (!data) {
        throw new Error('no Terms and Conditions Found')
      }
      res.json(data);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async fetchAllTermsOwner(req, res) {
    try {
      const terms = await TermsAndConditions.find({ type: "Owner" }).exec();
      let data = []
      terms.forEach(element => {
        if (!element.isCovid) {
          data.push(element)
        }
      });
      if (!data) {
        throw new Error('no Terms and Conditions Found')
      }
      res.json(data);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async fetchCovidOwner(req, res) {
    try {
      const terms = await TermsAndConditions.find({ type: "Owner" }).exec();
      let data = []
      terms.forEach(element => {
        if (element.isCovid) {
          data.push(element)
        }
      });
      if (!data) {
        throw new Error('no Terms and Conditions Found')
      }
      res.json(data);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async fetchAllFaqsOwner(req, res) {
    try {
      const data = await FAQ.find({ type: "Owner" }).exec();
      if (!data) {
        throw new Error('No Terms and Conditions Found')
      }
      res.json(data);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async fetchAllTermsUser(req, res) {
    try {
      const data = await TermsAndConditions.find({ type: "User" }).exec();
      if (!data) {
        throw new Error('no Terms and Conditions Found')
      }
      res.json(data);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async fetchAllFaqsUser(req, res) {
    try {
      const data = await FAQ.find({ type: "User" }).exec();
      if (!data) {
        throw new Error('no Terms and Conditions Found')
      }
      res.json(data);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async fetchAllReturns(req, res) {
    try {
      const data = await ReturnPolicy.find().exec();
      if (!data) {
        throw new Error('no Return Policies Found')
      }
      res.json(data);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async fetchAllContacts(req, res) {
    try {
      const data = await Contact.find().exec();
      if (!data) {
        throw new Error('no Contacts Found')
      }
      res.json(data);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async fetchContactById(req, res) {
    try {
      const data = await Contact.findById(req.params.id).exec();
      if (!data) {
        throw new Error('no Contact Found')
      }
      res.json(data);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }

  // =====================> UPDATE <===================== //
  async updatePolicies(req, res) {
    try {
      const result = await Policy.findByIdAndUpdate(req.params.id, req.body, {
        new: true
      });
      res.json({ result });
    } catch (error) {
      console.error(error.message);
      res.status(500).json({ error: error.message });
    }
  }
  async updateTerms(req, res) {
    try {
      const result = await TermsAndConditions.findByIdAndUpdate(req.params.id, req.body, {
        new: true
      });
      res.json({ result });
    } catch (error) {
      console.error(error.message);
      res.status(500).json({ error: error.message });
    }
  }
  async updateFaq(req, res) {
    try {
      const result = await FAQ.findByIdAndUpdate(req.params.id, req.body, {
        new: true
      });
      res.json({ result });
    } catch (error) {
      console.error(error.message);
      res.status(500).json({ error: error.message });
    }
  }
  async updateReturns(req, res) {
    try {
      const result = await ReturnPolicy.findByIdAndUpdate(req.params.id, req.body, {
        new: true
      });
      res.json({ result });
    } catch (error) {
      console.error(error.message);
      res.status(500).json({ error: error.message });
    }
  }

  // =====================> REMOVE <===================== //
  async removePolicyById(req, res) {
    try {
      const done = await Policy.deleteOne({ _id: req.params.id });
      if (!done) {
        throw new Error("Policy deletion failed, please provide correct id")
      }
      res.status(200).json({ succes: true })
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async removeTermsById(req, res) {
    try {
      const done = await TermsAndConditions.deleteOne({ _id: req.params.id });
      if (!done) {
        throw new Error("TermsAndConditions deletion failed, please provide correct id")
      }
      res.status(200).json({ succes: true })
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async removeFaqById(req, res) {
    try {
      const done = await FAQ.deleteOne({ _id: req.params.id });
      if (!done) {
        throw new Error("TermsAndConditions deletion failed, please provide correct id")
      }
      res.status(200).json({ succes: true })
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async removeReturnsById(req, res) {
    try {
      const done = await ReturnPolicy.deleteOne({ _id: req.params.id });
      if (!done) {
        throw new Error("Return Policy deletion failed, please provide correct id")
      }
      res.status(200).json({ succes: true })
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  async removeContactsById(req, res) {
    try {
      const done = await Contact.deleteOne({ _id: req.params.id });
      if (!done) {
        throw new Error("Contact deletion failed, please provide correct id")
      }
      res.status(200).json({ succes: true })
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }

}

module.exports = {
  pcfController: new PcfController()
};
