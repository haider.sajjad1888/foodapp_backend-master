const { Restaurant, AdminUser, Type, Booking, Notifications, DriverPayment, Order, Allergy, Driver, PushNotification, DriverOrders, MyIncome, DriverCharges } = require('../models');
const axios = require('axios').default;
const moment = require('moment')
const firebase = require("./../../config/firebase")

var FCM = require('fcm-node');
var serverKey = 'AAAA4eT6iKU:APA91bGnW-pS7akarc56ctfTg-918Q-7PLZ0aH5bjqSLH5PxzWmirWskFoBYhVIdbxz4Qet8te_DIPtUiou9s0RLnsHzoTzgAQ_f8LVEOAq3sakl6aQgmg6CfdvKNUDn6MLa60io_zP7'; //put your server key here
var fcm = new FCM(serverKey);
var ownerServerKey = 'AAAA9_jNx1w:APA91bHiD8OD7ySx90E8JlGUF51R5g8IoRhajgMuT1I2jI736H8TMDf9791EVBdSXd45YlRikBmsLgvTCHApPxWhnY6oP0fgQ5oFW0QKU6Ye3tzob32zcLFTLXSydZ8zN41wThtFq_lH'; //put your server key here
var ownerFcm = new FCM(ownerServerKey);

class RestaurantController {
	// =====================> CREATE <===================== //
	async createRestaurant(req, res) {
		try {
			const { userId, postCode } = req.body;
			const postCodeData = await axios.get(`https://api.postcodes.io/postcodes/${postCode}`);

			const restaurant = new Restaurant(req.body);
			restaurant.latitude = postCodeData.data.result.latitude
			restaurant.longitude = postCodeData.data.result.longitude
			await restaurant.save();
			if (!restaurant) {
				return res.status(400).json({ msg: 'restaurant didnt added' });
			}
			const addRes = await AdminUser.findByIdAndUpdate(userId, { $push: { restaurants: restaurant._id } }, { new: true })
				.populate('restaurants')
				.select('restaurants name')
				.exec();
			let notificationBody = {
				title: `${addRes.name}`,
				description: ` ${restaurant.name} created`,
				type: 'superadmin',
				userId
			}
			const notification = new Notifications(notificationBody);
			await notification.save();
			if (!notification) {
				return res.status(400).json({ msg: 'Notification not sent!' });
			}
			res.json({ data: addRes });
		} catch (error) {
			res.status(500).send(error.message);
		}
	}
	async searchRestaurants(req, res) {
		try {
			const { searchKey } = req.body
			const allRestaurants = await Restaurant.find().exec();
			const value = searchKey.toLowerCase();
			const owners = await AdminUser.find({ type: "owner" })
				.exec();
			const filter = allRestaurants.filter((item) => {
				const name = item.name.toLowerCase()
				if (name.includes(value)) {
					return item
				}
			})
			let data = []
			filter.forEach(restaurant => {
				owners.forEach(element => {
					if (element.restaurants.includes(restaurant._id)) {
						data.push({
							ownerId: element._id,
							restaurantId: restaurant._id,
							name: restaurant.name,
							logo: restaurant.logo
						})
					}
				});
			});
			res.json({ data });
		} catch (error) {
			res.status(500).send(error.message);
		}
	}
	async createDriverPayment(req, res) {
		try {
			const data = new DriverPayment(req.body);
			await data.save();
			if (!data) {
				return res.status(400).json({ msg: 'Payment did not added!' });
			}




			const driver = await Driver.findById(req.body.driverName)

			let ownernotificationBody = {
				title: driver.name,
				description: `applied for driver job`,
				type: 'owner',
				userId: req.body.owner,
				notificationFor: "job",
				data: JSON.stringify(data)
			}

			const ownerNotifi = firebase.database().ref(`${req.body.owner}`);
			const ownerNotifiId = ownerNotifi.push(ownernotificationBody).getKey(); // add notification and get its id
			const ownerNotifiItems = firebase.database().ref(`${req.body.owner}`);
			ownerNotifiItems.on('value', (snapshot) => {
				let items = snapshot.val();
				let newState = [];
				for (let item in items) {
					newState.push({
						id: item,
						title: items[item].title,
						userId: items[item].userId
					});
				}
				setTimeout(
					() => newState.map((item) => {
						if (item.id === ownerNotifiId) {
							const removeItems = firebase.database().ref(`/${req.body.owner}/${item.id}`);
							removeItems.remove();
						}
					}),
					10000
				);
			});

			const ownerNotification = new Notifications(ownernotificationBody);
			await ownerNotification.save();
			if (!ownerNotification) {
				return res.status(400).json({ msg: 'Notification not sent!' });
			}













			res.json({ data });
		} catch (error) {
			res.status(500).send(error.message);
		}
	}
	async createType(req, res) {
		try {
			const type = new Type(req.body);
			await type.save();
			if (!type) {
				return res.status(400).json({ msg: 'type didnt added' });
			}
			res.json({ type });
		} catch (error) {
			res.status(500).send(error.message);
		}
	}
	async createAllergy(req, res) {
		try {
			const type = new Allergy(req.body);
			await type.save();
			if (!type) {
				return res.status(400).json({ msg: 'Allergy was not added!' });
			}
			res.json({ type });
		} catch (error) {
			res.status(500).send(error.message);
		}
	}
	async createBooking(req, res) {
		try {
			const bookingDate = await Booking.find({ date: req.body.date, restaurantId: req.body.restaurantId }).exec();
			const bookingTime = req.body.date
			const currentTime = new Date
			if (moment(bookingTime).isSameOrBefore(currentTime)) {
				res.json({ error: "Invalid date is selected!" });
			} else {
				if (bookingDate) {
					let totalGuestAraival = 0
					bookingDate.forEach(element => {
						totalGuestAraival = totalGuestAraival + parseInt(element.guestNo)
					});
					const restaurant = await Restaurant.findById(req.body.restaurantId)
						.exec();
					let remainingCapacity = restaurant.capacity - totalGuestAraival
					if (req.body.guestNo <= remainingCapacity) {
						const booking = new Booking(req.body);
						await booking.save();
						if (!booking) {
							return res.status(400).json({ msg: 'booking didnt added' });
						}
						const owner = await AdminUser.find({ 'restaurants': booking.restaurantId })
							.exec();
						const restaurant = await Restaurant.findById(booking.restaurantId)
							.exec();
						const user = await AdminUser.findById(booking.userId)
							.exec();
						const pushNotification = await PushNotification.findOne({ userId: owner[0]._id }).exec();

						if (pushNotification) {
							var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
								to: pushNotification.fcm_token,
								// collapse_key: 'your_collapse_key',

								notification: {
									title: "New Booking",
									body: `You got new booking`,
									android_channel_id: "partner_app_channel",
									sound: "sound",
								},

								data: {  //you can send only notification or only data(or include both)
									notificationFor: "booking",
									data_string: JSON.stringify(booking)
								}
							};

							ownerFcm.send(message, function (err, response) {
								if (err) {
									console.log({ error: "Something has gone wrong!" });
								} else {
									console.log({ response });
								}
							});
						}
						let ownernotificationBody = {
							title: user.name,
							description: `requests for booking ${restaurant.name}.`,
							type: 'owner',
							userId: owner[0]._id,
							notificationFor: "booking",
							data: JSON.stringify(booking)
						}
						const itemsRef = firebase.database().ref(`${owner[0]._id}`);
						const newId = itemsRef.push(ownernotificationBody).getKey(); // add notification and get its id
						const getItems = firebase.database().ref(`${owner[0]._id}`);
						getItems.on('value', (snapshot) => {
							let items = snapshot.val();
							let newState = [];
							for (let item in items) {
								newState.push({
									id: item,
									title: items[item].title,
									userId: items[item].userId
								});
							}
							setTimeout(
								() => newState.map((item) => {
									if (item.id === newId) {
										const removeItems = firebase.database().ref(`/${owner[0]._id}/${item.id}`);
										removeItems.remove();
									}
								}),
								10000
							);
						});
						const ownerNotification = new Notifications(ownernotificationBody);
						await ownerNotification.save();
						if (!ownerNotification) {
							return res.status(400).json({ msg: 'Notification not sent!' });
						}

						const userPushNotification = await PushNotification.findOne({ userId: booking.userId }).exec();

						if (userPushNotification) {
							var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
								to: userPushNotification.fcm_token,
								// collapse_key: 'your_collapse_key',

								notification: {
									title: "Booking",
									body: `You booked ${restaurant.name}`
								},

								data: {  //you can send only notification or only data(or include both)
									notificationFor: "booking",
									data_string: JSON.stringify(booking)
								}
							};

							fcm.send(message, function (err, response) {
								if (err) {
									console.log({ error: "Something has gone wrong!" });
								} else {
									console.log({ response });
								}
							});
						}
						let usernotificationBody = {
							title: `You`,
							description: `request to booking ${restaurant.name}.`,
							type: 'user',
							userId: booking.userId,
							notificationFor: "booking",
						}

						const userItemsRef = firebase.database().ref(`${booking.userId}`);
						const userNewId = userItemsRef.push(usernotificationBody).getKey(); // add notification and get its id
						const userGetItems = firebase.database().ref(`${booking.userId}`);
						userGetItems.on('value', (snapshot) => {
							let items = snapshot.val();
							let newState = [];
							for (let item in items) {
								newState.push({
									id: item,
									title: items[item].title,
									userId: items[item].userId
								});
							}
							setTimeout(
								() => newState.map((item) => {
									if (item.id === userNewId) {
										const removeItems = firebase.database().ref(`/${booking.userId}/${item.id}`);
										removeItems.remove();
									}
								}),
								10000
							);
						});
						const userNotification = new Notifications(usernotificationBody);
						await userNotification.save();
						if (!userNotification) {
							return res.status(400).json({ msg: 'Notification not sent!' });
						}
						res.json({ booking });
					} else if (remainingCapacity === 0) {
						res.json({ error: "Sorry! All seats are booked, Please choose another date" });
					}
					else {
						res.json({ error: "Sorry! Our remaining seats on this date are: " + remainingCapacity });
					}
				}
			}

		} catch (error) {
			res.status(500).send(error.message);
		}
	}

	// =====================> FETCH <===================== //
	async fetchRestaurant(req, res) {
		try {
			const data = await Restaurant.findById(req.params.id)
				.populate('categories')
				.populate('drivers')
				.populate('driversNotifications')
				.exec();
			if (!data) {
				throw new Error("NO Restaurant Found")
			}
			const { openingTime, closingTime } = data;
			const date = moment(new Date()).format('hh:mm A')
			let isOpen = false;

			if (date > openingTime && date < closingTime) isOpen = true;
			const finalData = {
				data,
				isOpen
			}
			res.json(finalData);
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async findItsOwner(req, res) {
		try {
			const data = await AdminUser.find()
				.exec();
			let id = ''
			data.forEach(element => {
				element.restaurants.forEach(rest => {
					if (rest == req.params.id) {
						id = element._id
					}
				});
			});
			res.json(id);
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async riderCharges(req, res) {
		try {
			const id = req.params.id
			const user = await AdminUser.find({ _id: req.params.id })
				.populate('restaurants')
				.exec();
			let data = []
			for (let i = 0; i < user[0].restaurants.length; i++) {
				const id = user[0].restaurants[i]
				const payment = await DriverPayment.find({ restaurant: id, acceptedByOwner: true, acceptedByDriver: true })
					.populate('driverName')
					.populate('restaurant')
					.exec();
				if (payment.length) {
					data.push(payment)
				}
			}
			let drivers = []
			data.forEach(element => {
				drivers.push(element[0].driverName._id)
			});


			for (let j = 0; j < drivers.length; j++) {
				const driverId = drivers[j];

				const driverPayment = await DriverPayment.find({ driverName: driverId, acceptedByOwner: true, acceptedByDriver: true })
					.populate("restaurant")
					.exec();
				// console.log(driverPayment)
				if (driverPayment) {
					let earningData = []
					for (let i = 0; i < driverPayment.length; i++) {
						const element = driverPayment[i];
						let shiftCharges = parseInt(element.shiftCharge) || 0
						let chargePerHour = parseInt(element.chargePerHour) || 0
						let shiftStart = new Date("01/01/2007 " + element.shiftStart)
						let shiftEnd = new Date("01/01/2007 " + element.shiftEnd)
						let hourDiff = shiftEnd - shiftStart;
						let totalHours = (hourDiff / (1000 * 60 * 60)).toFixed(1)
						let earningFromShift = chargePerHour * parseInt(totalHours) + shiftCharges
						const orders = await Order.find({ driver: driverId, restaurant: element.restaurant._id, orderStatus: "delivered" })
							.populate('restaurant')
							// .populate('driver')
							.exec();

						let orderDistance = 0
						let deliveriesEarning = 0
						let areaLimit = driverPayment[0].areaLimit.replace(/\ Miles/g, '')
						for (let m = 0; m < orders.length; m++) {
							const order = orders[m];
							orderDistance = order.distance ? order.distance : 0
							if (orderDistance > areaLimit) {
								deliveriesEarning = deliveriesEarning + parseInt(element.perDeliveryOver)
							} else {
								deliveriesEarning = deliveriesEarning + parseInt(element.perDeliveryWithin)
							}
						}
						let earningFromDeliveries = deliveriesEarning
						let deliveryCharges = earningFromShift + earningFromDeliveries
						let finalData = {
							earningFromShift,
							earningFromDeliveries,
							chargePerHour,
							shiftCharges,
						}
						earningData.push(deliveryCharges)
					}
					let total = 0
					for (let k = 0; k < earningData.length; k++) {
						const earnings = earningData[k];
						total = total + earnings

					}
					res.json({ total });
				}
			}
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchPayment(req, res) {
		try {
			const data = await Restaurant.findById(req.params.id)
				.populate('restaurants')
				.exec();
			if (!data) {
				throw new Error("NO Restaurant Found")
			}
			res.json({ data });
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchAllRestaurant(req, res) {
		try {
			const data = await Restaurant.find({ isActive: true })
				.populate('categories', '_id name')
				.exec();
			if (!data.length > 0) {
				throw new Error("NO Restaurants Found")
			}
			res.json({ data });
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchAllUnAssignedRestaurant(req, res) {
		try {
			const data = await Restaurant.find({ isActive: true, isAssigned: false }).exec();
			if (!data.length > 0) {
				throw new Error("NO Restaurants Found")
			}
			res.json({ data });
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchEveryRestaurant(req, res) {
		try {
			const data = await Restaurant.find().exec();
			if (!data.length > 0) {
				throw new Error("NO Restaurants Found")
			}
			res.json({ data });
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchAllRestaurants(req, res) {
		try {
			const data = await Restaurant.find().exec();
			if (!data.length > 0) {
				throw new Error("NO Restaurants Found")
			}
			res.json({ data });
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchAllRestaurantsByType(req, res) {
		try {
			const type = req.query.type;
			const data = await Restaurant.find({ type: { $in: type }, isOpen: true }).exec();
			if (!data) {
				throw new Error("NO Restaurant by this Type")
			}
			res.json({ data });
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchDistance(req, res) {
		try {
			const postCode1 = req.query.postCode1;
			const postCode2 = req.query.postCode2;
			const userPostCodeData = await axios.get(`https://api.postcodes.io/postcodes/${postCode1}`);
			let userlatitude = userPostCodeData.data.result.latitude
			let userlongitude = userPostCodeData.data.result.longitude
			const restaurantPostCodeData = await axios.get(`https://api.postcodes.io/postcodes/${postCode2}`);
			let restaurantlatitude = restaurantPostCodeData.data.result.latitude
			let restaurantlongitude = restaurantPostCodeData.data.result.longitude

			var R = 6371; // km

			var dLat = (restaurantlatitude - userlatitude) * Math.PI / 180
			var dLon = (restaurantlongitude - userlongitude) * Math.PI / 180
			var lat1 = userlatitude * Math.PI / 180
			var lat2 = userlongitude * Math.PI / 180

			var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
				Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
			let distance = R * c;

			if (!distance) {
				res.json({ distance: 0 });
			}
			res.json({ distance });
		} catch (error) {
			res.status(500).send({ error: error.message })
		}
	}
	async fetchAllTypes(req, res) {
		try {
			const data = await Type.find().exec();
			if (data) {
				res.json({ data });
			} else {
				throw new Error('no Data');
			}
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchAllAllergies(req, res) {
		try {
			const data = await Allergy.find().exec();
			if (data) {
				res.json({ data });
			} else {
				throw new Error('No Data Found!');
			}
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchAllBookings(req, res) {
		try {
			let data = await Booking.find().exec();
			res.json({ data: data.reverse() });
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchBookingsCharges(req, res) {
		try {
			const id = req.params.id
			let data = await MyIncome.find({ userId: id, incomeType: "booking" }).exec();
			res.json({ data: data.reverse() });
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchAllPendingBookings(req, res) {
		try {
			let data = await Booking.find({ isAccepted: false }).exec();
			res.json({ data: data.reverse() });
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchAllBookingsByRestaurant(req, res) {
		try {
			const type = req.query.type;
			let data;

			if (type === "today") {
				const now = new Date();
				const startOfToday = new Date(now.getFullYear(), now.getMonth(), now.getDate());
				data = await Booking.find({
					restaurantId: req.params.id,
					createdAt: {
						$gte: startOfToday
					}
				}).exec();
			}
			if (type === "week") {
				data = await Booking.find({
					restaurantId: req.params.id,
					createdAt: {
						$gte: new Date(new Date() - 7 * 60 * 60 * 24 * 1000)
					}
				})
					.populate('restaurantId')
					.exec();
			}
			if (type === "month") {
				data = await Booking.find({
					restaurantId: req.params.id,
					createdAt: {
						$gte: new Date(new Date() - 30 * 60 * 60 * 24 * 1000)
					}
				})
					.populate('restaurantId')
					.exec();
			}
			res.json({ data });
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchCategoriesData(req, res) {
		try {
			const restaurantId = req.params.id
			const data = await Restaurant.findById(req.params.id)
				.populate({
					path: 'categories',
					populate: {
						path: 'foodItems',
						model: 'Food'
					}
				})
				.exec()
			res.json({ data });
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchBookingsByOwner(req, res) {
		try {
			const user = await AdminUser.find({ _id: req.params.id })
				.populate('restaurant')
				.exec();
			let data = []
			for (let i = 0; i < user[0].restaurants.length; i++) {
				const id = user[0].restaurants[i]
				const order = await Booking.find({ restaurantId: id, isAccepted: true })
					.populate('restaurantId')
					.exec();
				if (order.length) {
					data.push(order)
				}
			}
			if (!data[0]) {
				return res.json(data);
			}
			return res.json(data[0].reverse());
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchPendingBookingsByOwner(req, res) {
		try {
			const user = await AdminUser.find({ _id: req.params.id })
				.populate('restaurant')
				.exec();
			let data = []
			for (let i = 0; i < user[0].restaurants.length; i++) {
				const id = user[0].restaurants[i]
				const order = await Booking.find({ restaurantId: id, isAccepted: false })
					.populate('restaurantId')
					.exec();
				if (order.length) {
					data.push(order)
				}
			}
			if (!data[0]) {
				return res.json(data);
			}
			return res.json(data[0].reverse());
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchAllBookingsByUser(req, res) {
		try {
			const type = req.query.type;
			let data;

			if (type === "today") {
				const now = new Date();
				const startOfToday = new Date(now.getFullYear(), now.getMonth(), now.getDate());
				data = await Booking.find({
					userId: req.params.id,
					createdAt: {
						$gte: startOfToday
					}
				})
					.populate('restaurantId')
					.exec();
			}
			if (type === "week") {
				data = await Booking.find({
					userId: req.params.id,
					createdAt: {
						$gte: new Date(new Date() - 7 * 60 * 60 * 24 * 1000)
					}
				})
					.populate('restaurantId')
					.exec();
			}
			if (type === "month") {
				data = await Booking.find({
					userId: req.params.id,
					createdAt: {
						$gte: new Date(new Date() - 30 * 60 * 60 * 24 * 1000)
					}
				})
					.populate('restaurantId')
					.exec();
			}
			res.json({ data: data.reverse() });
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchOwnerRestaurants(req, res) {
		try {
			const data = await AdminUser.findById(req.params.id)
				.select('restaurants')
				.populate('restaurants')
				.exec();
			if (data) {
				res.json({ data });
			} else {
				throw new Error('no Data');
			}
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchOneTypes(req, res) {
		try {
			const data = await Type.findById(req.params.id).exec();
			if (data) {
				res.json({ data });
			} else {
				throw new Error('Data not found!');
			}
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchPaymentsByDriver(req, res) {
		try {
			const data = await DriverPayment.find({ driverName: req.params.id, acceptedByOwner: true, acceptedByDriver: false })
				.populate("restaurant")
				.exec();
			if (data) {
				res.json({ data });
			} else {
				throw new Error('Payments not found!');
			}
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchDriverEarnings(req, res) {
		try {
			const driverId = req.params.id
			const myOrders = await DriverOrders.find({ driver: driverId })
				.populate("restaurant")
				.exec();
			let data = []
			myOrders.forEach(element => {
				let deliveriesEarning = 0
				let orderDistance = element.distance ? element.distance : 0
				const restaurant = element.restaurant
				let areaLimit = restaurant.areaLimit ? restaurant.areaLimit.replace(/\ Miles/g, '') : 0
				if (orderDistance > areaLimit) {
					deliveriesEarning = restaurant.driverChargesWithin ? restaurant.driverChargesWithin : 0
				} else {
					deliveriesEarning = restaurant.driverChargesOver ? restaurant.driverChargesOver : 0
				}
				const earning = {
					date: element.createdAt,
					description: "Cr/Dl",
					ammount: deliveriesEarning
				}
				data.push(earning)
			});
			res.json({ data });
			// const driverPayment = await DriverPayment.find({ driverName: req.params.id, acceptedByOwner: true, acceptedByDriver: true })
			// 	.populate("restaurant")
			// 	.exec();
			// if (driverPayment) {
			// 	let data = []
			// 	for (let i = 0; i < driverPayment.length; i++) {
			// 		const element = driverPayment[i];
			// 		let shiftCharges = parseInt(element.shiftCharge)
			// 		let chargePerHour = parseInt(element.chargePerHour)
			// 		let shiftStart = new Date("01/01/2007 " + element.shiftStart)
			// 		let shiftEnd = new Date("01/01/2007 " + element.shiftEnd)
			// 		let hourDiff = shiftEnd - shiftStart;
			// 		let totalHours = (hourDiff / (1000 * 60 * 60)).toFixed(1)
			// 		let earningFromShift = chargePerHour * parseInt(totalHours) + shiftCharges
			// 		const orders = await Order.find({ driver: req.params.id, restaurant: element.restaurant._id, orderStatus: "delivered" })
			// 			.populate('restaurants')
			// 			.populate('driver')
			// 			.exec();
			// 		let orderDistance = 0;
			// 		let deliveriesEarning = 0
			// 		let areaLimit = driverPayment[0].areaLimit.replace(/\ Miles/g, '')
			// 		for (let m = 0; m < orders.length; m++) {
			// 			const order = orders[m];
			// 			orderDistance = order.distance ? order.distance : 0
			// 			if (orderDistance > areaLimit) {
			// 				deliveriesEarning = deliveriesEarning + parseInt(element.perDeliveryOver)
			// 			} else {
			// 				deliveriesEarning = deliveriesEarning + parseInt(element.perDeliveryWithin)
			// 			}
			// 		}
			// 		let finalData = {
			// 			paymentInfo: element,
			// 			earningFromShift,
			// 			earningFromDeliveries: deliveriesEarning,
			// 			chargePerHour,
			// 			hourDiff,
			// 			shiftCharges,
			// 			noDeliveries: orders.length
			// 		}
			// 		data.push(finalData)
			// 	}
			// 	res.json({ data });
			// } else {
			// 	throw new Error('Payments not found!');
			// }
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchDriverDeliveryIncome(req, res) {
		try {
			const id = req.params.id
			const orders = await Order.find({ driver: id })
				.populate("restaurant")
				.exec();
			let data = []
			orders.forEach(element => {
				if (element.distance > 3) {
					const order = {
						charges: element.restaurant.driverChargesOver,
						address: element.restaurant.address,
						restaurant: element.restaurant.name,
						orderNumber: element.orderNumber,
					}
					data.push(order)
				} else {
					const order = {
						charges: element.restaurant.driverChargesWithin,
						restaurant: element.restaurant.name,
						orderNumber: element.orderNumber,
					}
					data.push(order)
				}
			});
			res.json({ data });

		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchPaymentsListByDriver(req, res) {
		try {
			const data = await DriverPayment.find({ driverName: req.params.id, acceptedByOwner: true, acceptedByDriver: true })
				.populate("restaurant")
				.exec();
			if (data) {
				res.json({ data });
			} else {
				throw new Error('Payments not found!');
			}
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchPaymentsByOwner(req, res) {
		try {
			const user = await AdminUser.find({ _id: req.params.id })
				.populate('restaurants')
				.exec();
			let data = []
			for (let i = 0; i < user[0].restaurants.length; i++) {
				const id = user[0].restaurants[i]
				const payment = await DriverPayment.find({ restaurant: id, acceptedByOwner: false })
					.populate('driverName')
					.populate('restaurant')
					.exec();
				if (payment.length) {
					data.push(payment)
				}
			}
			if (!data[0]) {
				return res.json({ error: "No Requests Fonund!" });
			}
			return res.json(data[0]);
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchPaymentsListByOwner(req, res) {
		try {
			const user = await AdminUser.find({ _id: req.params.id })
				.populate('restaurants')
				.exec();
			let data = []
			for (let i = 0; i < user[0].restaurants.length; i++) {
				const id = user[0].restaurants[i]
				const payment = await DriverPayment.find({ restaurant: id, acceptedByOwner: true, acceptedByDriver: true })
					.populate('driverName')
					.populate('restaurant')
					.exec();
				if (payment.length) {
					data.push(payment)
				}
			}
			if (!data[0]) {
				return res.json({ error: "No Requests Fonund!" });
			}
			return res.json(data);
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchAllPayments(req, res) {
		try {
			const data = await DriverPayment.find().exec();
			if (data) {
				res.json({ data });
			} else {
				throw new Error('Payments not found!');
			}
		} catch (error) {
			res.status(500).send(error.message)
		}
	}

	async toRad(Value) {
		return Value * Math.PI / 180;
	}

	async getPostCode(req, res) {
		try {
			const postCode = req.query.postCode.toUpperCase()
			const primPostCode = postCode.substring(0, 2)


			const allRestaurants = await Restaurant.find({ isOpen: true, isActive: true })
				.populate("categories")
				.exec();
			let restaurants = []
			let allDistnces = []
			for (let i = 0; i < allRestaurants.length; i++) {
				const element = allRestaurants[i];

				if (primPostCode === element.postCode.substring(0, 2)) {
					const postCodeData = await axios.get(`https://api.postcodes.io/postcodes/${postCode}`);

					let latitude = postCodeData.data.result.latitude
					let longitude = postCodeData.data.result.longitude

					var R = 6371; // km

					var dLat = (element.latitude - latitude) * Math.PI / 180
					var dLon = (element.longitude - longitude) * Math.PI / 180
					var lat1 = latitude * Math.PI / 180
					var lat2 = longitude * Math.PI / 180

					var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
						Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
					var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
					let distance = R * c;

					let allData = {
						element, distance
					}

					restaurants.push(allData)

					let distances = {
						postCode: element.postCode,
						distanceInMiles: distance
					}
					allDistnces.push(distances)
				}
			}
			if (!restaurants) {
				throw new Error("No Restaurants Found")
			}
			res.json({ restaurants, allDistnces })
		} catch (error) {
			res.status(404).json({
				error: error.message
			})
		}
	}

	async getDeliveryFee(req, res) {
		try {


			const postCode1 = req.query.postCode1;
			const postCode2 = req.query.postCode2;
			const userPostCodeData = await axios.get(`https://api.postcodes.io/postcodes/${postCode1}`);
			let userlatitude = userPostCodeData.data.result.latitude
			let userlongitude = userPostCodeData.data.result.longitude
			const restaurantPostCodeData = await axios.get(`https://api.postcodes.io/postcodes/${postCode2}`);
			let restaurantlatitude = restaurantPostCodeData.data.result.latitude
			let restaurantlongitude = restaurantPostCodeData.data.result.longitude

			var R = 6371; // km

			var dLat = (restaurantlatitude - userlatitude) * Math.PI / 180
			var dLon = (restaurantlongitude - userlongitude) * Math.PI / 180
			var lat1 = userlatitude * Math.PI / 180
			var lat2 = userlongitude * Math.PI / 180

			var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
				Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
			let totalDistance = R * c;





			const driverCharges = await DriverCharges.find().exec();
			const distance = totalDistance ? parseInt(totalDistance) : 0
			let ammount = 0
			driverCharges.forEach(element => {
				const miles = parseInt(element.miles)
				if (distance >= miles) {
					ammount = parseFloat(element.charges)
				}
			});

			res.json({ ammount })
		} catch (error) {
			res.status(404).json({
				error: error.message
			})
		}
	}
	async findNearDrivers(req, res) {
		try {
			const id = req.query.id
			const restaurant = await Restaurant.find({ _id: id }).exec();
			const postCodes = await axios.get(`https://api.postcodes.io/postcodes/${restaurant[0].postCode}/nearest?radius=2000`);
			let postCodeData = postCodes.data.result
			let data = []
			for (let i = 0; i < postCodeData.length; i++) {
				const element = postCodeData[i];
				// const code = element.postcode.replace(/\s/g, '')
				const code = element.postcode
				const driver = await Driver.find({ workPlace: code })
				if (driver.length) {
					driver.forEach(element => {
						data.push(element)
					});
				}
			}
			res.json({ data })
		} catch (error) {
			res.status(404).json({
				error: error.message
			})
		}
	}
	async findNearRestaurants(req, res) {
		try {
			const postCode = req.query.postCode
			const postCodes = await axios.get(`https://api.postcodes.io/postcodes/${postCode}/nearest?radius=2000`);
			let postCodeData = postCodes.data.result
			let data = []
			for (let i = 0; i < postCodeData.length; i++) {
				const element = postCodeData[i];
				const code = element.postcode.replace(/\s/g, '')
				const driver = await Restaurant.find({ postCode: code }).exec()
				if (driver.length) {
					driver.forEach(element => {
						data.push(element)
					});
				}
			}
			res.json({ data })
		} catch (error) {
			res.status(404).json({
				error: error.message
			})
		}
	}
	async findNearRestaurantsLatLng(req, res) {
		try {
			const lat = req.query.lat
			const lon = req.query.lon
			const postCodes = await axios.get(`https://api.postcodes.io/postcodes?lon=${lon}&lat=${lat}/nearest?radius=2000`);
			if (postCodes.data.result.length) {
				const driverData = {
					workPlace: postCodes.data.result[0].postcode
				}
				const driverUpdate = await Driver.findByIdAndUpdate(req.query.id, driverData, {
					new: true
				}).exec();
			}
			let postCodeData = postCodes.data.result
			let data = []
			for (let i = 0; i < postCodeData.length; i++) {
				const element = postCodeData[i];
				const code = element.postcode.replace(/\s/g, '')
				const driver = await Restaurant.find({ postCode: code }).exec()
				if (driver.length) {
					driver.forEach(element => {
						data.push(element)
					});
				}
			}
			res.json({ data })
		} catch (error) {
			res.status(404).json({
				error: error.message
			})
		}
	}
	async getByPostCodeAndType(req, res) {
		try {
			const postCode = req.query.postCode.toUpperCase()
			const primPostCode = postCode.substring(0, 2)

			const allRestaurants = await Restaurant.find({ isOpen: true, isActive: true, type: req.query.type }).exec();
			let restaurants = []
			let allDistnces = []
			for (let i = 0; i < allRestaurants.length; i++) {
				const element = allRestaurants[i];

				if (primPostCode === element.postCode.substring(0, 2)) {
					const postCodeData = await axios.get(`https://api.postcodes.io/postcodes/${postCode}`);
					let latitude = postCodeData.data.result.latitude
					let longitude = postCodeData.data.result.longitude
					var R = 6371; // km
					var dLat = (element.latitude - latitude) * Math.PI / 180
					var dLon = (element.longitude - longitude) * Math.PI / 180
					var lat1 = latitude * Math.PI / 180
					var lat2 = longitude * Math.PI / 180

					var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
						Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
					var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
					let distance = R * c;

					let allData = {
						element, distance
					}

					restaurants.push(allData)
					let distances = {
						postCode: element.postCode,
						distanceInMiles: distance
					}
					allDistnces.push(distances)
				}
			}
			if (!restaurants) {
				throw new Error("No Restaurants Found")
			}
			res.json({ restaurants, allDistnces })
		} catch (error) {
			res.status(404).json({
				error: error.message
			})
		}
	}

	// =====================> UPDATE <===================== //
	async updateRestaurant(req, res) {
		try {
			const restaurantUpdate = await Restaurant.findByIdAndUpdate(req.params.id, req.body, {
				new: true
			}).exec();
			if (!restaurantUpdate) {
				throw new Error("Restaurant Update failed.")
			}
			res.json({ data: restaurantUpdate });
		}
		catch (error) {
			res.status(500).json({ error: error.message })
		}
	}
	async updatePostcode(req, res) {
		try {
			const { postCode } = req.body
			const postCodeData = await axios.get(`https://api.postcodes.io/postcodes/${postCode}`);
			const latitude = postCodeData.data.result.latitude
			const longitude = postCodeData.data.result.longitude

			const data = {
				latitude,
				longitude,
				postCode
			}

			const restaurantUpdate = await Restaurant.findByIdAndUpdate(req.params.id, data, {
				new: true
			}).exec();
			if (!restaurantUpdate) {
				throw new Error("Restaurant Update failed.")
			}
			res.json({ data: restaurantUpdate });
		}
		catch (error) {
			res.status(500).json({ error: error.message })
		}
	}
	async updatePayment(req, res) {
		try {
			const paymentUpdate = await DriverPayment.findByIdAndUpdate(req.params.id, req.body, {
				new: true
			}).exec();
			if (!paymentUpdate) {
				throw new Error("Restaurant Update failed.")
			}
			res.json({ data: paymentUpdate });
		}
		catch (error) {
			res.status(500).json({ error: error.message })
		}
	}
	async updateBooking(req, res) {
		try {
			const { isAccepted, ownerId } = req.body

			const booking = await Booking.findByIdAndUpdate(req.params.id, req.body, {
				new: true
			})
				.populate("restaurantId")
				.exec();
			const restaurant = await Restaurant.findById(booking.restaurantId._id).exec();
			const date = new Date;

			var a = moment(date);
			var b = moment(restaurant.createdAt);
			const diff = a.diff(b, 'days')
			if (isAccepted && diff > 60) {
				const ownerIncome = {
					userId: ownerId,
					userType: "owner",
					incomeData: JSON.stringify(booking),
					incomeType: "booking",
					totalIncome: 0.50,
				}
				const myIncome = new MyIncome(ownerIncome);
				await myIncome.save()
			}
			if (isAccepted) {
				let usernotificationBody = {
					title: `Your`,
					description: `booking is accepted by ${booking.restaurantId.name}.`,
					type: 'user',
					userId: booking.userId
				}
				const itemsRef = firebase.database().ref(`${booking.userId}`);
				const newId = itemsRef.push(usernotificationBody).getKey(); // add notification and get its id
				const getItems = firebase.database().ref(`${booking.userId}`);
				getItems.on('value', (snapshot) => {
					let items = snapshot.val();
					let newState = [];
					for (let item in items) {
						newState.push({
							id: item,
							title: items[item].title,
							userId: items[item].userId
						});
					}
					setTimeout(
						() => newState.map((item) => {
							if (item.id === newId) {
								const removeItems = firebase.database().ref(`/${booking.userId}/${item.id}`);
								removeItems.remove();
							}
						}),
						10000
					);
				});
				const userNotification = new Notifications(usernotificationBody);
				await userNotification.save();
				if (!userNotification) {
					return res.status(400).json({ msg: 'Notification not sent!' });
				}
			}
			if (!booking) {
				throw new Error("Booking Update failed.")
			}
			res.json({ data: booking });
		}
		catch (error) {
			res.status(500).json({ error: error.message })
		}
	}
	async updateRestaurantDriversList(req, res) {
		try {
			const { drivers } = req.body;
			const updateDriverList = await Restaurant.findByIdAndUpdate(req.params.id,
				{ "$pull": { driversNotifications: drivers }, "$addToSet": { drivers } }, { new: true }).exec();
			if (!updateDriverList) {
				throw new Error(" Add Driver in list failed.")
			}
			res.json({ data: updateDriverList });
		}
		catch (error) {
			res.status(500).json({ error: error.message })
		}
	}
	async updateRestaurantDriversNotifications(req, res) {
		try {
			const restaurantUpdate = await Restaurant.findByIdAndUpdate(req.params.id,
				{ $addToSet: { driversNotifications: req.body.drivers } }, { new: true }).exec();
			if (!restaurantUpdate) {
				throw new Error("Restaurant Update failed.")
			}
			res.json({ data: restaurantUpdate });

		}
		catch (error) {
			res.status(500).json({ error: error.message })
		}
	}
	async removeFromRestaurantDriversList(req, res) {
		try {
			const { drivers } = req.body;
			const removeDriverList = await Restaurant.findByIdAndUpdate(req.params.id,
				{ "$pull": { drivers } }, { new: true }).exec();
			if (!removeDriverList) {
				throw new Error("Remove Driver from  list failed.")
			}
			res.json({ data: removeDriverList });
		}
		catch (error) {
			res.status(500).json({ error: error.message })
		}
	}
	async removeFromRestaurantDriversNotification(req, res) {
		try {
			const { drivers } = req.body;
			const removeDriverNotification = await Restaurant.findByIdAndUpdate(req.params.id,
				{ "$pull": { driversNotifications: drivers } }, { new: true }).exec();
			if (!removeDriverNotification) {
				throw new Error("Remove Driver from notification list failed.")
			}
			res.json({ data: removeDriverNotification });

		}
		catch (error) {
			res.status(500).json({ error: error.message })
		}
	}
	async updateType(req, res) {
		try {
			const typeUpdate = await Type.findByIdAndUpdate(req.params.id, req.body, {
				new: true
			}).exec();
			if (typeUpdate) {
				res.json({ data: typeUpdate });
			} else {
				throw new Error(
					'Error: in  RestaurantController => updateRestaurant'
				);
			}
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async addFoodToRestaurant(req, res) {
		try {
			const { foodOptions } = req.body;
			const restaurantUpdate = await Restaurant.findByIdAndUpdate(req.params.id, { $push: { foodOptions } }, {
				new: true
			}).exec();
			if (restaurantUpdate) {
				res.json({ data: restaurantUpdate });
			} else {
				throw new Error(
					'Error: in  RestaurantController => updateRestaurant'
				);
			}
		} catch (error) {
			res.status(500).send(error.message)
		}
	}

	// =====================> REMOVE <===================== //
	async removeRestaurant(req, res) {
		try {
			const done = await Restaurant.deleteOne({ _id: req.params.id }).exec();
			if (done) {
				res.json('Restaurant Deleted')
			} else {
				throw { message: "Error: RestaurantController.removeRestaurant", done }
			}
		} catch (error) {
			res.status(500).send(error.message);
		}
	}
	async removePayment(req, res) {
		try {
			const done = await DriverPayment.deleteOne({ _id: req.params.id }).exec();
			if (done) {
				res.json({ result: 'Payment Deleted' })
			} else {
				throw { message: "Error: RestaurantController.removeRestaurant", done }
			}
		} catch (error) {
			res.status(500).send(error.message);
		}
	}
	async removeType(req, res) {
		try {
			const done = await Type.findByIdAndDelete(req.params.id).exec();
			if (done) {
				res.json({ done })
			} else {
				throw { message: "Error: RestaurantController.removeType", done }
			}
		} catch (error) {
			res.status(500).send(error.message);
		}
	}
	async removeAllergy(req, res) {
		try {
			const done = await Allergy.findByIdAndDelete(req.params.id).exec();
			if (done) {
				res.json({ done })
			} else {
				throw { message: "Error: RestaurantController.removeAllergy", done }
			}
		} catch (error) {
			res.status(500).send(error.message);
		}
	}
	async removeBooking(req, res) {
		try {
			const booking = await Booking.findById(req.params.id).exec();
			const done = await Booking.deleteOne({ _id: req.params.id });

			if (!booking.isAccepted) {

				let usernotificationBody = {
					title: `Your`,
					description: `order is declined by ${booking.restaurantId}`,
					type: 'user',
					userId: booking.userId,
					notifictioFor: "booking",
					data: JSON.stringify(booking)
				}
				const assignItemsRef = firebase.database().ref(`${booking.userId}`);
				const assignNewId = assignItemsRef.push(usernotificationBody).getKey(); // add notification and get its id
				const assignGetItems = firebase.database().ref(`${booking.userId}`);
				assignGetItems.on('value', (snapshot) => {
					let items = snapshot.val();
					let newState = [];
					for (let item in items) {
						newState.push({
							id: item,
							title: items[item].title,
							userId: items[item].userId
						});
					}
					setTimeout(
						() => newState.map((item) => {
							if (item.id === assignNewId) {
								const removeItems = firebase.database().ref(`/${booking.userId}/${item.id}`);
								removeItems.remove();
							}
						}),
						10000
					);
				});
				const userNotification = new Notifications(usernotificationBody);
				await userNotification.save();
				if (!userNotification) {
					return res.status(400).json({ msg: 'Notification not sent!' });
				}
				const pushNotification = await PushNotification.findOne({ userId: booking.userId }).exec();

				if (pushNotification) {
					var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
						to: pushNotification.fcm_token,
						// collapse_key: 'your_collapse_key',

						notification: {
							title: "Notification",
							body: `Your booking is denied by ${booking.restaurantId}`
						},

						data: {  //you can send only notification or only data(or include both)
							notificationFor: "booking",
							data_string: JSON.stringify(booking)
						}
					};

					fcm.send(message, function (err, response) {
						if (err) {
							console.log({ error: "Something has gone wrong!" });
						} else {
							console.log({ response });
						}
					});
				}
			}
			if (!done) {
				throw new Error("Booking deletion failed, please provide correct id")
			}
			res.json({ result: 'Booking has been deleted.' })
		} catch (error) {
			res.status(500).send(error.message);
		}
	}
	async declineBooking(req, res) {
		try {
			const data = req.body
			const { userId } = req.body
			const itemsRef = firebase.database().ref(`${userId}`);
			const newId = itemsRef.push(data).getKey(); // add notification and get its id
			const getItems = firebase.database().ref(`${userId}`);
			getItems.on('value', (snapshot) => {
				let items = snapshot.val();
				let newState = [];
				for (let item in items) {
					newState.push({
						id: item,
						title: items[item].title,
						userId: items[item].userId
					});
				}
				setTimeout(
					() => newState.map((item) => {
						if (item.id === newId) {
							const removeItems = firebase.database().ref(`/${userId}/${item.id}`);
							removeItems.remove();
						}
					}),
					10000
				);
			});
			const userNotification = new Notifications(data);
			await userNotification.save();
			if (!userNotification) {
				return res.status(400).json({ msg: 'Notification not sent!' });
			}
			const done = await Booking.deleteOne({ _id: req.params.id });
			if (!done) {
				throw new Error("Booking deletion failed, please provide correct id")
			}
			res.json({ result: 'Booking has been deleted.' })
		} catch (error) {
			res.status(500).send(error.message);
		}
	}

}


module.exports = {
	restaurantController: new RestaurantController()
};
