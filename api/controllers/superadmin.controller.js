const { Role, SuperAdmin, Permissions, DashboardPermissions, AdminUser, Driver, Invoice, MyIncome, DriverOrders, InHouse } = require('../models');
var nodemailer = require('nodemailer');
const sgTransport = require("nodemailer-sendgrid-transport");
const bcrypt = require('bcrypt');
const moment = require('moment');
const { InvoiceInfo, Sales, DriverCharges, DriverIncome } = require('../models/super-admin.model');
const firebase = require("./../../config/firebase")

var FCM = require('fcm-node');
var serverKey = 'AAAA9_jNx1w:APA91bHiD8OD7ySx90E8JlGUF51R5g8IoRhajgMuT1I2jI736H8TMDf9791EVBdSXd45YlRikBmsLgvTCHApPxWhnY6oP0fgQ5oFW0QKU6Ye3tzob32zcLFTLXSydZ8zN41wThtFq_lH'; //put your server key here
var fcm = new FCM(serverKey);
var driverServerKey = 'AAAA4eT6iKU:APA91bGnW-pS7akarc56ctfTg-918Q-7PLZ0aH5bjqSLH5PxzWmirWskFoBYhVIdbxz4Qet8te_DIPtUiou9s0RLnsHzoTzgAQ_f8LVEOAq3sakl6aQgmg6CfdvKNUDn6MLa60io_zP7'; //put your server key here
var driver_fcm = new FCM(driverServerKey);

class SuperAdminController {
	// =====================> CREATE <===================== //
	async createRole(req, res) {
		try {
			const role = new Role(req.body);
			await role.save();
			if (!role) {
				return res.status(400).json({ msg: 'no role added.' });
			}
			res.json({ role });
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async sendMessage(req, res) {
		try {
			const { id, senderId, message, name, userType } = req.body
			const time = "" + new Date
			const data = {
				senderId,
				message,
				time,
				isSeen: false,
				name: name ? name : '',
				// userType: userType ? userType : ''
			}
			if (id === "live-support") {
				const itemsRef = firebase.database().ref(`${id}chat`).child(userType).child(senderId);
				const newId = itemsRef.push(data).getKey();
				const getItems = firebase.database().ref(`${id}chat`).child(userType).child(senderId);
				getItems.on('value', (snapshot) => {
					let items = snapshot.val();
					let newState = [];
					for (let item in items) {
						newState.push({
							id: item
						});
					}
					setTimeout(
						() => newState.map((item) => {
							if (item.id === newId) {
								const removeItems = firebase.database().ref(`/${id}chat`).child(userType).child(`/${senderId}/${item.id}`);
								console.log(removeItems)
								removeItems.remove();
							}
						}),
						43200000
					);
				});
			} else {
				const itemsRef = firebase.database().ref(`${id}chat`).child(senderId);
				const newId = itemsRef.push(data).getKey();
				const getItems = firebase.database().ref(`${id}chat`).child(senderId);
				getItems.on('value', (snapshot) => {
					let items = snapshot.val();
					let newState = [];
					for (let item in items) {
						newState.push({
							id: item
						});
					}
					setTimeout(
						() => newState.map((item) => {
							if (item.id === newId) {
								const removeItems = firebase.database().ref(`/${id}chat`).child(`/${senderId}/${item.id}`);
								console.log(removeItems)
								removeItems.remove();
							}
						}),
						43200000
					);
				});
			}
			if (senderId === "live-support") {
				const recieverItemsRef = firebase.database().ref(`${senderId}chat`).child(userType).child(id);
				const recieverNewId = recieverItemsRef.push(data).getKey();
				const recievergetItems = firebase.database().ref(`${senderId}chat`).child(userType).child(id);
				recievergetItems.on('value', (snapshot) => {
					let items = snapshot.val();
					let recieverNewState = [];
					for (let item in items) {
						recieverNewState.push({
							id: item
						});
					}
					setTimeout(
						() => recieverNewState.map((item) => {
							if (item.id === recieverNewId) {
								const removeItems = firebase.database().ref(`/${senderId}chat`).child(userType).child(`/${id}/${item.id}`);
								removeItems.remove();
							}
						}),
						43200000
					);
				});
			} else {
				const recieverItemsRef = firebase.database().ref(`${senderId}chat`).child(id);
				const recieverNewId = recieverItemsRef.push(data).getKey();
				const recievergetItems = firebase.database().ref(`${senderId}chat`).child(id);
				recievergetItems.on('value', (snapshot) => {
					let items = snapshot.val();
					let recieverNewState = [];
					for (let item in items) {
						recieverNewState.push({
							id: item
						});
					}
					setTimeout(
						() => recieverNewState.map((item) => {
							if (item.id === recieverNewId) {
								const removeItems = firebase.database().ref(`/${senderId}chat`).child(`/${id}/${item.id}`);
								removeItems.remove();
							}
						}),
						43200000
					);
				});
			}
			res.json({ result: "Message Sent!" });
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async sendNotification(req, res) {
		try {
			var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
				to: req.body.fcm_token,
				// collapse_key: 'your_collapse_key',

				notification: {
					title: req.body.title,
					body: req.body.notificationBody,
					android_channel_id: "123",
				},


				data: {  //you can send only notification or only data(or include both)
					notificationFor: "testing",
					data_string: req.body.data_string,
					// actions: [
					// 	{
					// 		inline: true,
					// 		callback: "accept",
					// 		foreground: false,
					// 		title: "Accept"
					// 	},
					// 	{
					// 		icon: "snooze",
					// 		callback: "reject",
					// 		foreground: false,
					// 		title: "Reject"
					// 	}
					// ]
				}
			};

			// fcm.send(message, function (err, response) {
			driver_fcm.send(message, function (err, response) {
				if (err) {
					console.log({ error: "Something has gone wrong with push notification!" });
					res.json({ error: "Something has gone wrong with push notification!" });
				} else {
					// console.log({ response });
					res.json({ response });
				}
			});

		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}

	async resetPassword(req, res) {
		try {
			const { reciever, Model } = req.body;
			let user = null
			if (Model === "SuperAdmin") {
				user = await SuperAdmin.findOne({ email: reciever })
			} else if (Model === "Driver") {
				user = await Driver.findOne({ email: reciever })
			} else if (Model === "AdminUser") {
				user = await AdminUser.findOne({ email: reciever })
			}
			if (!user) {
				throw new Error('No Such Account Exists');
			}
			const resetNumber = Math.random().toFixed(36).substring(2, 4) + Math.random().toFixed(36).substring(2, 4);
			if (Model === "SuperAdmin") {
				await SuperAdmin.findByIdAndUpdate(user._id, { resetPassword: resetNumber }, {
					new: true
				});
			} else if (Model === "Driver") {
				await Driver.findByIdAndUpdate(user._id, { resetPassword: resetNumber }, {
					new: true
				});
			} else if (Model === "AdminUser") {
				await AdminUser.findByIdAndUpdate(user._id, { resetPassword: resetNumber }, {
					new: true
				});
			}
			let transporter = nodemailer.createTransport({
				service: 'gmail',
				auth: {
					user: 'foodappsuk@gmail.com',
					pass: 'business13579'
				}
			});
			const mailOptions = {
				from: 'foodappsuk@gmail.com',
				to: reciever,
				subject: 'Reset Password',
				html: `< br >< !DOCTYPE html > <html><head>
                <title>Reset Password</title>
              </head>
                <body> <p>Click <a href='https://foodapps.uk/reset-password?code=${resetNumber}&id=${user._id}&Model=${Model}'>here</a> to reset password!</p>
                </body></html>`
			}

			transporter.sendMail(mailOptions, function (error, info) {
				if (error) {
					console.log(error)
					res.json({ message: error.message })
				}
				else {
					console.log('Email sent: ' + info.response)
					res.json({ status: 200, message: `Email sent: ${info.response}` })
				}
			})
		}
		catch (error) {
			res.status(500).json({ error: error.message });
		}
	}

	async updatePassword(req, res) {
		try {
			const { resetCode, id, Model, password } = req.body;
			if (!resetCode || resetCode === '') {
				throw new Error('Please Enter Reset Code.')
			}
			let user = null
			if (Model === "SuperAdmin") {
				user = await SuperAdmin.findOne({ _id: id, resetPassword: resetCode }).exec();
			} else if (Model === "Driver") {
				user = await Driver.findOne({ _id: id, resetPassword: resetCode }).exec();
			} else if (Model === "AdminUser") {
				user = await AdminUser.findOne({ _id: id, resetPassword: resetCode }).exec();
			}
			if (!user) {
				throw new Error('Password Verification failed.')
			}
			const salt = await bcrypt.genSalt(10);
			const newPassword = await bcrypt.hash(password, salt);
			let updated = null
			if (Model === "SuperAdmin") {
				updated = await SuperAdmin.findByIdAndUpdate(id, { password: newPassword, resetPassword: '' }, {
					new: true
				});
			} else if (Model === "Driver") {
				updated = await Driver.findByIdAndUpdate(id, { password: newPassword, resetPassword: '' }, {
					new: true
				});
			} else if (Model === "AdminUser") {
				updated = await AdminUser.findByIdAndUpdate(id, { password: newPassword, resetPassword: '' }, {
					new: true
				});
			}
			if (!updated) {
				throw new Error("password cannot update")
			}
			res.status(200).json({ message: "password updated successfully" })
		}
		catch (error) {
			res.status(500).json({ error: error.message });
		}
	}

	async createPermissions(req, res) {
		try {
			const role = new Permissions(req.body);
			await role.save();
			if (!role) {
				return res.status(400).json({ msg: 'no permission added.' });
			}
			res.json({ role });
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async createInvoice(req, res) {
		try {
			const invoice = new Invoice(req.body);
			await invoice.save();
			if (!invoice) {
				return res.status(400).json({ msg: 'Invoice was not added.' });
			}
			res.json({ invoice });
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async createDriverCharges(req, res) {
		try {
			const data = new DriverCharges(req.body);
			await data.save();
			if (!data) {
				return res.status(400).json({ msg: 'Info was not added.' });
			}
			res.json({ data });
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async createSale(req, res) {
		try {
			const Sale = new Sales(req.body);
			await Sale.save();
			if (!Sale) {
				return res.status(400).json({ msg: 'Sale was not added.' });
			}
			res.json({ Sale });
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async addMaintenanceMessage(req, res) {
		try {



			const message = req.body.maintenanceMessage
			let notificationBody = {
				title: `Notification for maintenance`,
				description: message,
				announce: false,
			}
			const itemsRef = firebase.database().ref(`maintenance-notification`);
			const newId = itemsRef.push(notificationBody).getKey(); // add notification and get its id
			res.json({ result: "newId " });
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async createInvoiceInfo(req, res) {
		try {
			const invoiceInfo = new InvoiceInfo(req.body);
			await invoiceInfo.save();
			if (!invoiceInfo) {
				return res.status(400).json({ msg: 'Invoice info was not added.' });
			}
			res.json({ invoiceInfo });
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async createDashboardPermissions(req, res) {
		try {
			const role = new DashboardPermissions(req.body);
			await role.save();
			if (!role) {
				return res.status(400).json({ msg: 'no permission added.' });
			}
			res.json({ role });
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}

	// =====================> FETCH <===================== //
	async fetchAllRoles(req, res) {
		try {
			const data = await Role.find().exec();
			if (!data) {
				throw new Error('no Roles Found')
			}
			res.json(data);
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async fetchInvoice(req, res) {
		try {
			const data = await Invoice.find({ user: req.params.id }).exec();
			res.json(data);
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async fetchSalesReports(req, res) {
		try {
			const startDate = moment().startOf('isoWeek').format("DD MMM YYYY")
			const endDate = moment().format("DD MMM YYYY")
			const data = await MyIncome.find({
				userId: req.params.id,
				createdAt: {
					$gte: startDate,
					$lte: endDate
				}
			}).exec();
			res.json(data);
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async fetchCurrentMonthInvoices(req, res) {
		try {
			const id = req.params.id
			const week = req.query.week
			const date = new Date

			const year = moment(date).format("YYYY")
			const firstDay = moment(year).add(week - 1, 'weeks').startOf('week').format() // "02 04 2017"m, gives you Sunday(last day of the week)
			const lastDay = moment(year).add(week - 1, 'weeks').endOf('week').format()
			const weekRecord = await MyIncome.find({
				userId: id,
				createdAt: {
					$gte: firstDay,
					$lte: lastDay
				}
			}).exec();
			if (weekRecord.length) {
				res.json({ data: weekRecord });
			}
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async fetchInvoicesByDates(req, res) {
		try {
			const id = req.params.id
			const startDate = req.query.startDate
			const endDate = req.query.endDate


			const invoices = await MyIncome.find({
				userId: id,
				incomeType: "order",
				createdAt: {
					$gte: startDate,
					$lte: endDate
				}
			}).exec();
			const bookingCharges = await MyIncome.find({
				userId: id,
				incomeType: "booking",
				createdAt: {
					$gte: startDate,
					$lte: endDate
				}
			}).exec();
			const owner = await AdminUser.findById(id)
				.populate("restaurants")
				.exec();

			const driverCharges = await DriverIncome.find({
				restaurant: owner.restaurants[0]._id,
				createdAt: {
					$gte: startDate,
					$lte: endDate
				}
			}).exec();
			const finalData = {
				invoices,
				driverCharges,
				bookingCharges
			}
			if (invoices.length) {
				res.json({ data: finalData });
			} else {
				res.json({ error: "No invoice found" });
			}
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async fetchDriverInvoicesByDates(req, res) {
		try {
			const id = req.params.id
			const startDate = req.query.startDate
			const endDate = req.query.endDate

			const invoices = await MyIncome.find({
				userId: id,
				incomeType: "order",
				createdAt: {
					$gte: startDate,
					$lte: endDate
				}
			}).exec();
			// const bookingCharges = await MyIncome.find({
			// 	userId: id,
			// 	incomeType: "booking",
			// 	createdAt: {
			// 		$gte: startDate,
			// 		$lte: endDate
			// 	}
			// }).exec();
			// const owner = await AdminUser.findById(id)
			// 	.populate("restaurants")
			// 	.exec();

			// const driverCharges = await DriverIncome.find({
			// 	restaurant: owner.restaurants[0]._id,
			// 	createdAt: {
			// 		$gte: startDate,
			// 		$lte: endDate
			// 	}
			// }).exec();
			const finalData = {
				invoices,
				// driverCharges,
				// bookingCharges
			}
			if (invoices.length) {
				res.json({ data: finalData });
			} else {
				res.json({ error: "No invoice found" });
			}
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}

	async fetchInhouseDriverCharges(req, res) {
		try {
			const id = req.params.id
			const updateDriverOrder = await InHouse.find({ restaurant: id, orderType: "in-house" })
				.populate("restaurant")
				.exec();
			if (updateDriverOrder.length) {
				res.json({ data: updateDriverOrder });
			} else {
				res.json({ error: "No driver charges found!" });
			}
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async fetchWeeksOfCurrentMonth(req, res) {
		try {
			const id = req.params.id
			const date = new Date
			const weekOfCurrentMonth = (moment().week() - (moment().month() * 4))

			let data = []
			for (let i = 0; i < weekOfCurrentMonth; i++) {
				const year = moment(date).format("YYYY")
				const firstDay = moment(year).add(i, 'weeks').startOf('week').format() // "02 04 2017"m, gives you Sunday(last day of the week)
				const lastDay = moment(year).add(i, 'weeks').endOf('week').format()
				const weekRecord = await MyIncome.find({
					userId: id,
					createdAt: {
						$gte: firstDay,
						$lte: lastDay
					}
				}).exec();
				const week = {
					weekNumber: i + 1,
					firstDay: moment(firstDay).format("DD-MM-YYYY"),
					lastDay: moment(lastDay).format("DD-MM-YYYY"),
					transactions: weekRecord.length
				}

				data.push(week)
			}
			res.json(data);
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async fetchInvoiceInfo(req, res) {
		try {
			const data = await InvoiceInfo.find({ userId: req.params.id }).exec();
			if (!data[0]) {
				res.json({});
			}
			res.json(data[0]);
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async fetchAllPermissions(req, res) {
		try {
			const data = await Permissions.find().exec();
			if (!data) {
				throw new Error('no permission Found')
			}
			res.json(data);
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async fetchAllDashboardPermissions(req, res) {
		try {
			const data = await DashboardPermissions.find().exec();
			if (!data) {
				throw new Error('no permission Found')
			}
			res.json(data);
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async fetchAllSuperAdmin(req, res) {
		try {
			const data = await SuperAdmin.find().exec();
			if (!data) {
				throw new Error('no SuperAdmin Found')
			}
			res.json(data);
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async fetchAllDriverCharges(req, res) {
		try {
			const data = await DriverCharges.find().exec();
			if (!data) {
				throw new Error('no SuperAdmin Found')
			}
			res.json(data);
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async fetchSuperAdminById(req, res) {
		try {
			const data = await SuperAdmin.findById(req.params.id)
				.populate('jobPosition')
				.populate("restaurants")
				.exec();
			if (!data) {
				throw new Error('no SuperAdmin Found')
			}
			res.json(data);
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async fetchRolsById(req, res) {
		try {
			const data = await Role.findById(req.params.id).exec();
			if (!data) {
				throw new Error('No Role Found')
			}
			res.json(data);
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async fetchRoleByUserId(req, res) {
		try {
			const data = await SuperAdmin.findById(req.params.id)
				.select('jobPosition')
				.populate('jobPosition')
				.exec();
			if (data) {
				res.json({ data });
			} else {
				throw new Error('no Data');
			}
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	async fetchChat(req, res) {
		try {
			const { id } = req.body
			const getItems = firebase.database().ref(id);
			getItems.on('value', (snapshot) => {
				let items = snapshot.val();
				let data = [];
				for (let item in items) {
					data.push({
						id: item,
						senderId: items[item].senderId,
						message: items[item].message
					});
				}
				res.json({ data })
			});
		} catch (error) {
			res.status(500).send(error.message)
		}
	}
	// =====================> UPDATE <===================== //
	async updateSuperAdmin(req, res) {
		try {
			const result = await SuperAdmin.findByIdAndUpdate(req.params.id, req.body, {
				new: true
			});
			res.json({ data: result });
		} catch (error) {
			console.error(error.message);
			res.status(500).json({ error: error.message });
		}
	}
	async updateInvoiceInfo(req, res) {
		try {
			const result = await InvoiceInfo.findByIdAndUpdate(req.params.id, req.body, {
				new: true
			});
			res.json({ data: result });
		} catch (error) {
			console.error(error.message);
			res.status(500).json({ error: error.message });
		}
	}
	async updateRole(req, res) {
		try {
			const result = await Role.findByIdAndUpdate(req.params.id, req.body, {
				new: true
			});
			res.json({ data: result });
		} catch (error) {
			console.error(error.message);
			res.status(500).json({ error: error.message });
		}
	}
	async updateDriverCharges(req, res) {
		try {
			const result = await DriverCharges.findByIdAndUpdate(req.params.id, req.body, {
				new: true
			});
			res.json({ data: result });
		} catch (error) {
			console.error(error.message);
			res.status(500).json({ error: error.message });
		}
	}
	async updateRolePermission(req, res) {
		try {
			const result = await Role.findByIdAndUpdate(req.params.id, req.body, {
				new: true
			});
			res.json({ data: result });
		} catch (error) {
			console.error(error.message);
			res.status(500).json({ error: error.message });
		}
	}


	// =====================> REMOVE <===================== //
	async removeRoleById(req, res) {
		try {
			const done = await Role.findByIdAndDelete(req.params.id);
			if (!done) {
				throw new Error("Role deletion failed, please provide correct id")
			}
			res.json({ sucess: true })
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async removeDriverCharges(req, res) {
		try {
			const done = await DriverCharges.findByIdAndDelete(req.params.id);
			if (!done) {
				throw new Error("Info deletion failed, please provide correct id")
			}
			res.json({ sucess: true })
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async removeSuperAdminById(req, res) {
		try {
			const done = await SuperAdmin.findByIdAndDelete(req.params.id);
			if (!done) {
				throw new Error("SuperAdmin deletion failed, please provide correct id")
			}
			res.json({ sucess: true })
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async removeChats(req, res) {
		try {

			res.json({ sucess: "chat removed" })
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}
	async removePermissions(req, res) {
		try {
			const done = await Permissions.findByIdAndDelete(req.params.id);
			if (!done) {
				throw new Error("Permissions deletion failed, please provide correct id")
			}
			res.json({ sucess: true })
		} catch (error) {
			res.status(500).json({ error: error.message });
		}
	}

}

module.exports = {
	superAdminController: new SuperAdminController()
};
