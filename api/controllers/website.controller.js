const { Caraousel } = require('../models');


class WebsiteController {
    // =====================> CREATE <===================== //
    async createCaraousel(req, res) {
        try {
            const data = new Caraousel(req.body);
            await data.save();
            if (!data) {
                return res.status(400).json({ msg: 'Caraousel was not added' });
            }
            res.json({ data });
        } catch (error) {
            res.status(500).json({ error: error.message });
        }
    }
    async fetchAllCaraousels(req, res) {
        try {
            const data = await Caraousel.find().exec();
            if (!data) {
                throw new Error('No Caraousel Found!');
            }
            res.json({ data });
        } catch (error) {
            res.json({
                error: error.message
            });
        }
    }

    async removeCaraousel(req, res) {
        try {
            const done = await Caraousel.findByIdAndRemove(req.params.id).exec();
            if (!done) {
                throw new Error("Could Not Delete the Caraousel")
            }
            res.json('Carousel Deleted!')
        } catch (error) {
            res.status(500).send(error.message);
        }
    }
    async updateCaraousel(req, res) {
        try {
            const data = await Caraousel.findByIdAndUpdate(req.params.id, req.body, {
                new: true
            }).exec();
            if (data) {
                res.json({ data });
            } else {
                throw new Error(
                    'Carousel was not updated!'
                );
            }
        } catch (error) {
            res.status(500).json({ error: error.message });
        }
    }
}

module.exports = {
    websiteController: new WebsiteController()
};
