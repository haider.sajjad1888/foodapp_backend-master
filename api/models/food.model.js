const mongoose = require('mongoose');
const { Schema } = require('mongoose');

const common = {
  type: String,
  required: true,
  trim: true
};
const defaults = {
  type: String,
  default: ""
}

const SpecialOfferSchema = new Schema({
  name: { ...common },
  description: { ...common },
  packages: [{
    chooseAny: String,
    name: String,
    catId: String,
    products: [String]
  }],
  price: { type: Number, required: true },
  restaurant: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Restaurant'
  },
  isActive: { type: Boolean, default: true },
}, { timestamps: true });

const FoodSchema = new Schema({
  type: { ...defaults },
  title: { ...common },
  products: { ...defaults },
  options: [{
    name: String,
    price: String
  }],
  description: { ...defaults },
  price: { type: Number, required: true },
  foodAllergy: [{
    icon: String,
    name: String,
    _id: String,
  }],
  isActive: { type: Boolean, default: true },
}, { timestamps: true });

const CartSchema = new Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'AdminUser'
  },
  restaurant: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Restaurant'
  },
  count: { ...defaults },
  price: { ...defaults },
  foodItems: {
    _id: String,
    title: String,
    option: {
      _id: String,
      name: String,
      price: String
    },
    price: String,
    quantity: String,
    products: String,
    allergy: [String],
  },
}, { timestamps: true });

const CategorySchema = new Schema({
  name: { ...common },
  foodItems: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Food'
  }]
});

const Food = mongoose.model('Food', FoodSchema);
const Cart = mongoose.model('Cart', CartSchema);
const SpecialOffer = mongoose.model('SpecialOffer', SpecialOfferSchema);
const Category = mongoose.model('Category', CategorySchema);

module.exports = {
  Food,
  Cart,
  SpecialOffer,
  Category
}