const { AdminUser, Driver, Notifications, DriverPayment, GhostUser, PushNotification } = require('./user.model');
const { SuperAdmin, Role, WebCommission, Permissions, DashboardPermissions, Invoice, InvoiceInfo, Sales, DriverCharges, DriverIncome } = require('./super-admin.model');
const { TermsAndConditions, FAQ, ReturnPolicy, Policy, Contact } = require('./pcf.model');
const { Food, Category, SpecialOffer, Cart } = require('./food.model');
const { Restaurant, Type, Booking, Allergy } = require('./restaurant.model');
const { Order, DriverOrders, InHouse, MyIncome } = require('./order.model');
const { Caraousel } = require('./website.model');



module.exports = {
  AdminUser,
  TermsAndConditions,
  ReturnPolicy,
  MyIncome,
  Notifications,
  InHouse,
  Allergy,
  Policy,
  GhostUser,
  Sales,
  PushNotification,
  DriverCharges,
  DriverIncome,
  InvoiceInfo,
  Food,
  Category,
  Cart,
  Invoice,
  Restaurant,
  Driver,
  DriverPayment,
  WebCommission,
  Order,
  SpecialOffer,
  SuperAdmin,
  Type,
  DriverOrders,
  Contact,
  Caraousel,
  Role,
  Permissions,
  FAQ,
  DashboardPermissions,
  Booking
}