const mongoose = require('mongoose');
const { Schema } = require('mongoose');

const common = {
  type: String,
  required: true,
  trim: true
};
const defaults = {
  type: String,
  default: ""
}

const OrderSchema = new Schema({
  orderNumber: { ...defaults },
  address: { ...defaults },
  deliveryTime: { ...defaults },
  phoneNumber: { ...defaults },
  deliveryCharge: { ...defaults },
  recievedTime: { ...defaults },
  pickupTime: { ...defaults },
  distance: { ...defaults },
  discount: { ...defaults },
  kitchenNotes: { ...defaults },
  type: { ...defaults },
  orderType: { ...defaults },
  products: { ...defaults },
  deliveredAt: { ...defaults },
  riderServices: { type: Boolean },
  paymentMethod: {
    type: String,
    default: "Cash"
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'AdminUser'
  },
  driver: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Driver'
  },
  restaurant: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Restaurant'
  },
  foodItems: [{
    title: String,
    option: String,
    price: String,
    quantity: String,
    products: [String],
    allergy: [String],
  }],
  orderStatus: { ...defaults },
  totalPrice: { type: Number },
  // expireAt: {
  //   type: Date,
  //   default: Date.now,
  //   index: { expires: '30 days' },
  // },
}, { timestamps: true });

const InHouseSchema = new Schema({
  restaurant: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Restaurant'
  },
  name: { ...defaults },
  orderNumber: { ...defaults },
  address: { ...defaults },
  phoneNumber: { ...defaults },
  deliveryCharges: { ...defaults },
  deliveryTime: { ...defaults },
  recievedTime: { ...defaults },
  pickupTime: { ...defaults },
  deliveredAt: { ...defaults },
  orderType: { ...defaults },
  kitchenNotes: { ...defaults },
  distance: { ...defaults },
  driver: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Driver'
  },
  orderStatus: { ...defaults },
  totalPrice: { type: Number },
  paymentMethod: {
    type: String,
    default: "Cash"
  },
  // type: { ...defaults },
  // products: { ...defaults },
  // foodItems: [{
  //   title: String,
  //   option: String,
  //   price: String,
  //   quantity: String,
  //   products: [String],
  //   allergy: [String],
  // }],
  // expireAt: {
  //   type: Date,
  //   default: Date.now,
  //   index: { expires: '30 days' },
  // },
}, { timestamps: true });

const DriverOrdersSchema = new Schema({
  orderNumber: { ...defaults },
  address: { ...defaults },
  deliveryTime: { ...defaults },
  phoneNumber: { ...defaults },
  deliveryCharge: { ...defaults },
  recievedTime: { ...defaults },
  pickupTime: { ...defaults },
  customerName: { ...defaults },
  deliveredAt: { ...defaults },
  distance: { ...defaults },
  orderType: { ...defaults },
  paymentMethod: {
    type: String,
    default: "Cash"
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'AdminUser'
  },
  orderId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Order'
  },
  driver: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Driver'
  },
  restaurant: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Restaurant'
  },
  foodItems: [{
    title: String,
    option: String,
    price: String,
    quantity: String,
    allergy: [String],
  }],
  orderStatus: { ...defaults },
  totalPrice: { type: Number },
  // expireAt: {
  //   type: Date,
  //   default: Date.now,
  //   index: { expires: '30 days' },
  // },
}, { timestamps: true });

const MyIncomeSchema = new Schema({
  userId: { ...defaults },
  userType: { ...defaults },
  incomeData: { ...defaults },
  incomeType: { ...defaults },
  totalIncome: { type: Number },
}, { timestamps: true });

const Order = mongoose.model('Order', OrderSchema);
const MyIncome = mongoose.model('MyIncome', MyIncomeSchema);
const InHouse = mongoose.model('InHouse', InHouseSchema);
const DriverOrders = mongoose.model('DriverOrders', DriverOrdersSchema);

module.exports = {
  Order,
  MyIncome,
  InHouse,
  DriverOrders
}