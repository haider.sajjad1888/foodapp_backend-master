const mongoose = require('mongoose');
const { Schema } = require('mongoose');

const PolicySchema = new Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  }
}, { timestamps: true });

const TermAndConditionSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  isCovid: { type: Boolean, default: false },
  description: {
    type: String,
    required: true
  }
}, { timestamps: true });

const FAQSchema = new Schema({
  question: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  answer: {
    type: String,
    required: true
  }
}, { timestamps: true });

const ReturnPolicySchema = new Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  }
}, { timestamps: true });

const ContactSchema = new Schema({
  name: {
    type: String,
    default: ""
  },
  email: {
    type: String,
    required: true
  },
  message: {
    type: String,
    required: true
  }
}, { timestamps: true });

const TermsAndConditions = mongoose.model('TermsAndConditions', TermAndConditionSchema);
const FAQ = mongoose.model('FAQ', FAQSchema);
const Policy = mongoose.model('Policy', PolicySchema);
const ReturnPolicy = mongoose.model('ReturnPolicy', ReturnPolicySchema);
const Contact = mongoose.model('Contact', ContactSchema);


module.exports = {
  Policy,
  TermsAndConditions,
  ReturnPolicy,
  Contact,
  FAQ
};
