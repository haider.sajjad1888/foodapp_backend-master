const mongoose = require('mongoose');
const { Schema } = require('mongoose');

const common = {
  type: String,
  required: true,
  trim: true
};
const defaults = {
  type: String,
  default: ""
}

const RestaurantSchema = new Schema({
  logo: { ...defaults },
  banner: { ...defaults },
  name: { ...common },
  address: { ...common },
  city: { ...defaults },
  latitude: { ...defaults },
  longitude: { ...defaults },
  postCode: {
    type: String,
    uppercase: true,
    trim: true,
    default: ""
  },
  speciality: [{ ...defaults }],
  discount: { ...defaults },
  discountType: { ...defaults },
  deliveryCharges: { ...common },
  openingTime: { ...common },
  closingTime: { ...common },
  pickupTime: { ...defaults },
  bookingAvailableFrom: { ...defaults },
  bookingAvailableTo: { ...defaults },
  deliveryTime: [{ ...defaults }],
  minOrder: { type: Number, required: true },
  freeDelivery: { type: Boolean },
  deliveryAvailable: { type: Boolean },
  isOpen: { type: Boolean, default: true },
  riderServices: { type: Boolean, default: true },
  booking: { type: Boolean, default: true },
  isAssigned: { type: Boolean, default: false },
  capacity: { type: Number },
  areaLimit: { ...common },
  commission: { ...defaults },
  type: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Type'
  }],
  categories: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category'
  }],
  packages: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'SpecialOffer'
  }],
  driversNotifications: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Driver'
  }],
  drivers: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Driver'
  }],
  isActive: { type: Boolean, default: true },
  isPermitted: { type: Boolean, default: false },
  collectionAvailability: { type: Boolean, default: false },
}, { timestamps: true });

const TypeSchema = new Schema({
  type: { ...common },
  logo: { ...defaults },
  banner: { ...defaults },
});

const AllergySchema = new Schema({
  name: { ...common },
  icon: { ...defaults },
});

const BookingSchema = new Schema({
  name: { ...common },
  bookingFor: { ...common },
  guestNo: { ...common },
  date: { ...common },
  isAccepted: { type: Boolean, default: false },
  bookingFrom: { ...defaults },
  bookingTo: { ...defaults },
  contact: { ...defaults },
  ownerId: { ...defaults },
  restaurantId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Restaurant'
  },
  userId: { ...defaults },
}, { timestamps: true });

const Restaurant = mongoose.model('Restaurant', RestaurantSchema);
const Type = mongoose.model('Type', TypeSchema);
const Allergy = mongoose.model('Allergy', AllergySchema);
const Booking = mongoose.model('Booking', BookingSchema);

module.exports = {
  Restaurant,
  Type,
  Allergy,
  Booking
}