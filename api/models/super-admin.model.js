const mongoose = require('mongoose');
const { Schema } = require('mongoose');

const common = {
  type: String,
  required: true,
  trim: true
};
const defaults = {
  type: String,
  default: ""
}

const SuperAdminSchema = new Schema({
  name: { ...defaults },
  email: { ...common },
  password: { ...common },
  contact: { ...defaults },
  jobPosition: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Role'
  },
  restaurants: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Restaurant'
  }],
  address: { ...defaults },
  idCardNumber: { ...defaults },
  joiningDate: { ...defaults },
  resetPassword: { ...defaults },
  salary: { ...defaults },
  commission: { ...defaults },
  isActive: { type: Boolean, default: true },
}, { timestamps: true });

const RoleSchema = new Schema({
  name: { ...common },
  permissions: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Role'
  }],
  dashboardPermissions: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'DashboardPermissions'
  }],
  isActive: { type: Boolean, default: true },
});

const PermissionsSchema = new Schema({
  name: { ...common },
});

const DashboardPermissionsSchema = new Schema({
  name: { ...common },
});

const WebCommissionSchema = new Schema({
  ammount: { ...common },
  userId: { ...common },
  userType: { ...common },
  commissionData: { ...common },
});

const SalesSchema = new Schema({
  ammount: { ...common },
  userId: { ...common },
  userType: { ...common },
  salesData: { ...common },
});

const InvoiceSchema = new Schema({
  user: { ...common },
  type: { ...common },
  date: { ...common },
  description: { ...common },
  amount: { ...common },
});

const DriverChargesSchema = new Schema({
  miles: { ...common },
  charges: { ...common },
  isActive: { type: Boolean, default: true },
}, { timestamps: true });


const InvoiceInfoSchema = new Schema({
  name: { ...common },
  userType: { ...common },
  invoiceType: { ...common },
  userId: { ...common },
  invoices: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Invoice'
  }],
  bankName: { ...common },
  sortCode: { ...common },
  accountNumber: { ...common },
});

const DriverIncomeSchema = new Schema({
  driver: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Driver'
  },
  restaurant: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Restaurant'
  },
  ammount: { ...common },
  isActive: { type: Boolean, default: true },
}, { timestamps: true });

const SuperAdmin = mongoose.model('SuperAdmin', SuperAdminSchema);
const WebCommission = mongoose.model('WebCommission', WebCommissionSchema);
const DriverIncome = mongoose.model('DriverIncome', DriverIncomeSchema);
const DriverCharges = mongoose.model('DriverCharges', DriverChargesSchema);
const Invoice = mongoose.model('Invoice', InvoiceSchema);
const Sales = mongoose.model('Sales', SalesSchema);
const InvoiceInfo = mongoose.model('InvoiceInfo', InvoiceInfoSchema);
const Permissions = mongoose.model('Permissions', PermissionsSchema);
const DashboardPermissions = mongoose.model('DashboardPermissions', DashboardPermissionsSchema);
const Role = mongoose.model('Role', RoleSchema);

module.exports = {
  SuperAdmin,
  Permissions,
  WebCommission,
  Invoice,
  DriverIncome,
  DriverCharges,
  Sales,
  InvoiceInfo,
  DashboardPermissions,
  Role,
}