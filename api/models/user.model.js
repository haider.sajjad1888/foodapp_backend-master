const mongoose = require('mongoose');
const { Schema } = require('mongoose');

const common = {
  type: String,
  required: true,
  trim: true
};
const defaults = {
  type: String,
  default: ""
}

const AdminUserSchema = new Schema({
  type: { ...common },
  name: { ...defaults },
  email: { ...common },
  mobile: { ...defaults },
  UTRNumber: { ...defaults },
  hygieneCertificateNumber: { ...defaults },
  password: { ...common },
  resetPassword: { ...defaults },
  postCode: { ...defaults },
  address: { ...defaults },
  cardNumber: { ...defaults },
  restaurants: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Restaurant'
  }],
  cart: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Cart'
  }],
  favRestaurants: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Restaurant'
  }],
  isActive: { type: Boolean, default: true }
}, { timestamps: true });

const GhostUserSchema = new Schema({
  name: { ...defaults },
  email: { ...common },
  mobile: { ...common },
  type: { ...common },
  password: { ...common },
  resetPassword: { ...defaults },
  postCode: { ...defaults },
  address: { ...defaults },
  cardNumber: { ...defaults },
  restaurants: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Restaurant'
  }],
  cart: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Cart'
  }],
  favRestaurants: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Restaurant'
  }],
  isActive: { type: Boolean, default: true }
}, { timestamps: true });

const DriverSchema = new Schema({
  name: { ...defaults },
  image: { ...defaults },
  email: { ...common },
  password: { ...common },
  UTRno: { ...defaults },
  number: { ...defaults },
  address: { ...defaults },
  postCode: { ...defaults },
  workPlace: { ...defaults },
  idNumber: { ...defaults },
  idImage: { ...defaults },
  regNo: { ...defaults },
  totalEarning: { type: Number },
  rating: { type: Number },
  resetPassword: { ...defaults },
  isAvailable: { type: Boolean, default: false },
  newCommer: { type: Boolean, default: true },
  comission: { ...defaults },
  rideWith: { ...defaults },
  licenseType: { ...defaults },
  insurance: { ...defaults },
  restaurants: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Restaurant'
  }],
  isActive: { type: Boolean, default: true }
}, { timestamps: true });

const DriverPaymentSchema = new Schema({
  driverName: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Driver'
  },
  restaurant: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Restaurant'
  },
  chargePerHour: { ...defaults },
  shiftCharge: { ...defaults },
  shiftStart: { ...defaults },
  shiftEnd: { ...defaults },
  areaLimit: { ...defaults },
  perDeliveryWithin: { ...defaults },
  perDeliveryOver: { ...defaults },
  acceptedByDriver: { type: Boolean, default: false },
  acceptedByOwner: { type: Boolean, default: false },
  isActive: { type: Boolean, default: true }
}, { timestamps: true });

const NotificationsSchema = new Schema({
  title: { ...defaults },
  description: { ...defaults },
  type: { ...defaults },
  notifictioFor: { ...defaults },
  data: { ...defaults },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'AdminUser'
  },
  restaurantId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Restaurant'
  },
  driverId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Driver'
  },
  isSeen: { type: Boolean, default: false },
  isNewAdded: { type: Boolean, default: true }
}, { timestamps: true });

const PushNotificationSchema = new Schema({
  deviceId: { ...common },
  userId: { ...common },
  fcm_token: { ...common },
}, { timestamps: true });

const Notifications = mongoose.model('Notifications', NotificationsSchema);
const PushNotification = mongoose.model('PushNotification', PushNotificationSchema);
const AdminUser = mongoose.model('AdminUser', AdminUserSchema);
const Driver = mongoose.model('Driver', DriverSchema);
const DriverPayment = mongoose.model('DriverPayment', DriverPaymentSchema);
const GhostUser = mongoose.model('GhostUser', GhostUserSchema);


module.exports = {
  AdminUser,
  PushNotification,
  Driver,
  DriverPayment,
  GhostUser,
  Notifications
}