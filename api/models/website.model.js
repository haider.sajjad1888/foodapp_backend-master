const mongoose = require('mongoose');
const { Schema } = require('mongoose');

const common = {
    type: String,
    required: true,
    trim: true
};
const defaults = {
    type: String,
    default: ""
}

const CaraouselSchema = new Schema({
    title: { ...common },
    buttonTitle: { ...common },
    logo: { ...common },
    color: { ...common },
    link: { ...common },
}, { timestamps: true });

const Caraousel = mongoose.model('Caraousel', CaraouselSchema);

module.exports = {
    Caraousel
}