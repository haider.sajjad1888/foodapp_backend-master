const express = require('express');
const auth = require('../middleware/auth')
const upload = require('../../config/multer.img');

const {
  adminController,
  pcfController,
  foodController,
  restaurantController,
  orderController,
  superAdminController,
  websiteController
} = require('./../controllers');


//                                                                            admin routes

const adminRoutes = express.Router();
adminRoutes.get('/fetch-all-users', adminController.fetchAllUsers);
adminRoutes.get('/fetch-html', adminController.fetchHtml);
adminRoutes.get('/fetch-all-owners', adminController.fetchAllOwners);
adminRoutes.get('/fetch-all-drivers', adminController.fetchAllDrivers);
adminRoutes.get('/fetch-all-devices', adminController.fetchAllDevices);
adminRoutes.get('/fetch-devices-by-userid/:id', adminController.fetchDevicesByUserId);
adminRoutes.get('/fetch-all-super-admin', adminController.fetchAllSuperAdmin);
adminRoutes.get('/fetch/:id', adminController.fetchAdminUsersById);
adminRoutes.get('/fetch-drivers-stats/:id', adminController.fetchDriversStats);
adminRoutes.get('/fetch-owner/:id', adminController.fetchOwnerById);
adminRoutes.get('/fetch-driver/:id', adminController.fetchDriverById);
adminRoutes.get('/fetch-fav-res/:id', adminController.fetchAllFavRestaurants);
adminRoutes.get('/fetch-dashboard-data', adminController.fetchAllDashboardData);
adminRoutes.post('/login', adminController.login);
adminRoutes.post('/login-ghost', adminController.ghostLogin);
adminRoutes.post('/login-driver', adminController.driverLogin);
adminRoutes.post('/login-super-admin', adminController.loginSuperAdmin);
adminRoutes.post('/create', adminController.signUp);
adminRoutes.post('/create-ghost', adminController.ghostSignUp);
adminRoutes.post('/create-super-admin', adminController.signUpSuperAdmin);
adminRoutes.post('/create-driver', adminController.driverSignUp);
adminRoutes.post('/add-device', adminController.addDevice);
adminRoutes.post('/base64-upload', adminController.base64Upload);
adminRoutes.post('/create-driver-notification', adminController.createDriverNotification);
adminRoutes.post('/payment', adminController.stripePayment);
adminRoutes.post('/paypal-payment', adminController.paypalPayment);
adminRoutes.post('/upload-image', upload.single("image"), adminController.uploadImage);
adminRoutes.put('/update/:id', adminController.updateUser);
adminRoutes.put('/update-driver/:id', adminController.updateDriver);
adminRoutes.put('/add-fav-res/:id', adminController.addFavoriteRestaurants);
adminRoutes.delete('/remove-user/:id', adminController.removeUserById);
adminRoutes.delete('/remove-driver/:id', adminController.removeDriverById);


//                                                                            super admin routes
const superAdminRoutes = express.Router();
superAdminRoutes.get('/roles', superAdminController.fetchAllRoles);
superAdminRoutes.get('/invoices/:id', superAdminController.fetchInvoice);
superAdminRoutes.get('/fetch-sales-reports/:id', superAdminController.fetchSalesReports);
superAdminRoutes.get('/fetch-current-month-invoices/:id', superAdminController.fetchCurrentMonthInvoices);
superAdminRoutes.get('/fetch-invoices-by-dates/:id', superAdminController.fetchInvoicesByDates);
superAdminRoutes.get('/fetch-driver-invoices-by-dates/:id', superAdminController.fetchDriverInvoicesByDates);
superAdminRoutes.get('/fetch-inhouse-driver-charges/:id', superAdminController.fetchInhouseDriverCharges);
superAdminRoutes.get('/fetch-current-month-weeks/:id', superAdminController.fetchWeeksOfCurrentMonth);
superAdminRoutes.get('/invoice-info/:id', superAdminController.fetchInvoiceInfo);
superAdminRoutes.get('/permissions', superAdminController.fetchAllPermissions);
superAdminRoutes.get('/dashboard-permissions', superAdminController.fetchAllDashboardPermissions);
superAdminRoutes.get('/fetch', superAdminController.fetchAllSuperAdmin);
superAdminRoutes.get('/fetch-driver-charges', superAdminController.fetchAllDriverCharges);
superAdminRoutes.get('/fetch/:id', superAdminController.fetchSuperAdminById);
superAdminRoutes.get('/role-by-id/:id', superAdminController.fetchRolsById);
superAdminRoutes.get('/role-by-user-id/:id', superAdminController.fetchRoleByUserId);
superAdminRoutes.get('/get-chat', superAdminController.fetchChat);
superAdminRoutes.get('/notifications', adminController.fetchAllNotifications);
superAdminRoutes.get('/notification/:id', adminController.fetchNotificationById);
superAdminRoutes.get('/owner-notifications/:id', adminController.fetchNotificationByOwnerId);
superAdminRoutes.get('/new-owner-notifications/:id', adminController.fetchNewOwnerNotifications);
superAdminRoutes.get('/new-driver-notifications/:id', adminController.fetchNewDriverNotifications);
superAdminRoutes.get('/driver-notifications/:id', adminController.fetchNotificationByDriverId);
superAdminRoutes.get('/restaurant-commission', adminController.fetchRestaurantCommission);
superAdminRoutes.get('/restaurant-revenue/:id', adminController.fetchRestaurantRevenue);
superAdminRoutes.get('/driver-commission', adminController.fetchDriverCommission);
superAdminRoutes.get('/superadmin-notifications', adminController.fetchNotificationBySuperAdmin);
superAdminRoutes.get('/fetch-my-income/:id', adminController.fetchMyIncome);
superAdminRoutes.get('/superadmin-new-notifications', adminController.fetchNewNotificationBySuperAdmin);
superAdminRoutes.post('/create-role', superAdminController.createRole);
superAdminRoutes.post('/send-message', superAdminController.sendMessage);
superAdminRoutes.post('/send-notification', superAdminController.sendNotification);
superAdminRoutes.post('/reset-password', superAdminController.resetPassword);
superAdminRoutes.post('/update-password', superAdminController.updatePassword);
superAdminRoutes.post('/create-permissions', superAdminController.createPermissions);
superAdminRoutes.post('/create-sale', superAdminController.createSale);
superAdminRoutes.post('/add-maintenance-message', superAdminController.addMaintenanceMessage);
superAdminRoutes.post('/create-invoice', superAdminController.createInvoice);
superAdminRoutes.post('/create-driver-charges', superAdminController.createDriverCharges);
superAdminRoutes.post('/create-invoice-info', superAdminController.createInvoiceInfo);
superAdminRoutes.post('/create-dashboard-permissions', superAdminController.createDashboardPermissions);
superAdminRoutes.post('/create-notification', adminController.createNotification);
superAdminRoutes.put('/update/:id', superAdminController.updateSuperAdmin);
superAdminRoutes.put('/update-invoice-info/:id', superAdminController.updateInvoiceInfo);
superAdminRoutes.put('/update-role/:id', superAdminController.updateRole);
superAdminRoutes.put('/update-driver-charges/:id', superAdminController.updateDriverCharges);
superAdminRoutes.put('/update-role-permission/:id', superAdminController.updateRolePermission);
superAdminRoutes.put('/update-notification/:id', adminController.updateNotification);
superAdminRoutes.delete('/remove-role/:id', superAdminController.removeRoleById);
superAdminRoutes.delete('/remove/:id', superAdminController.removeSuperAdminById);
superAdminRoutes.delete('/remove-driver-charges/:id', superAdminController.removeDriverCharges);
superAdminRoutes.delete('/remove-chats', superAdminController.removeChats);
superAdminRoutes.delete('/remove-permissions/:id', superAdminController.removePermissions);
superAdminRoutes.delete('/notification/:id', adminController.deleteNotification);


//                                                                            restaurant routes

const restaurantRoutes = express.Router();
restaurantRoutes.get('/fetch/:id', restaurantController.fetchRestaurant)
restaurantRoutes.get('/find-its-owner/:id', restaurantController.findItsOwner)
restaurantRoutes.get('/rider-charges/:id', restaurantController.riderCharges)
restaurantRoutes.get('/payment/:id', restaurantController.fetchPayment)
restaurantRoutes.get('/fetch', restaurantController.fetchAllRestaurant)
restaurantRoutes.get('/fetch-unassigned', restaurantController.fetchAllUnAssignedRestaurant)
restaurantRoutes.get('/fetch-every-restaurant', restaurantController.fetchEveryRestaurant)
restaurantRoutes.get('/fetch-all', restaurantController.fetchAllRestaurants)
restaurantRoutes.get('/fetch-type', restaurantController.fetchAllTypes)
restaurantRoutes.get('/fetch-allergies', restaurantController.fetchAllAllergies)
restaurantRoutes.get('/fetch-one-type/:id', restaurantController.fetchOneTypes)
restaurantRoutes.get('/fetch-driver-payments/:id', restaurantController.fetchPaymentsByDriver)
restaurantRoutes.get('/fetch-driver-earnings/:id', restaurantController.fetchDriverEarnings)
restaurantRoutes.get('/fetch-driver-delivery-income/:id', restaurantController.fetchDriverDeliveryIncome)
restaurantRoutes.get('/fetch-driver-payments-list/:id', restaurantController.fetchPaymentsListByDriver)
restaurantRoutes.get('/fetch-payment-owner/:id', restaurantController.fetchPaymentsByOwner)
restaurantRoutes.get('/fetch-payment-list-owner/:id', restaurantController.fetchPaymentsListByOwner)
restaurantRoutes.get('/fetch-payments', restaurantController.fetchAllPayments)
restaurantRoutes.get('/fetch-by-type', restaurantController.fetchAllRestaurantsByType)
restaurantRoutes.get('/fetch-distance', restaurantController.fetchDistance)
restaurantRoutes.get('/fetch-by-owner/:id', restaurantController.fetchOwnerRestaurants)
restaurantRoutes.get('/fetch-booking', restaurantController.fetchAllBookings)
restaurantRoutes.get('/fetch-booking-charges/:id', restaurantController.fetchBookingsCharges)
restaurantRoutes.get('/fetch-pending-booking', restaurantController.fetchAllPendingBookings)
restaurantRoutes.get('/fetch-booking-restaurant/:id', restaurantController.fetchAllBookingsByRestaurant)
restaurantRoutes.get('/fetch-categories-data/:id', restaurantController.fetchCategoriesData)
restaurantRoutes.get('/fetch-booking-user/:id', restaurantController.fetchAllBookingsByUser)
restaurantRoutes.get('/fetch-booking-owner/:id', restaurantController.fetchBookingsByOwner)
restaurantRoutes.get('/fetch-pending-booking/:id', restaurantController.fetchPendingBookingsByOwner)
restaurantRoutes.get('/fetch-code', restaurantController.getPostCode)
restaurantRoutes.get('/fetch-delivery-fee', restaurantController.getDeliveryFee)
restaurantRoutes.get('/fetch-near-drivers', restaurantController.findNearDrivers)
restaurantRoutes.get('/fetch-near-restaurants', restaurantController.findNearRestaurants)
restaurantRoutes.get('/fetch-near-restaurants-latlong', restaurantController.findNearRestaurantsLatLng)
restaurantRoutes.get('/fetch-type-code', restaurantController.getByPostCodeAndType)
restaurantRoutes.post('/create', restaurantController.createRestaurant)
restaurantRoutes.post('/search-restaurants', restaurantController.searchRestaurants)
restaurantRoutes.post('/create-driver-payment', restaurantController.createDriverPayment)
restaurantRoutes.post('/create-type', restaurantController.createType)
restaurantRoutes.post('/create-allergy', restaurantController.createAllergy)
restaurantRoutes.post('/create-booking', restaurantController.createBooking)
restaurantRoutes.put('/update/:id', restaurantController.updateRestaurant)
restaurantRoutes.put('/update-postcode/:id', restaurantController.updatePostcode)
restaurantRoutes.put('/update-payment/:id', restaurantController.updatePayment)
restaurantRoutes.put('/update-booking/:id', restaurantController.updateBooking)
restaurantRoutes.put('/update-type/:id', restaurantController.updateType)
restaurantRoutes.put('/driver-list/:id', restaurantController.updateRestaurantDriversList)
restaurantRoutes.put('/driver-notification-list/:id', restaurantController.updateRestaurantDriversNotifications)
restaurantRoutes.put('/remove-driver-list/:id', restaurantController.removeFromRestaurantDriversList)
restaurantRoutes.put('/remove-driver-notification-list/:id', restaurantController.removeFromRestaurantDriversNotification)
restaurantRoutes.put('/add-food/:id', restaurantController.addFoodToRestaurant)
restaurantRoutes.delete('/remove/:id', restaurantController.removeRestaurant)
restaurantRoutes.delete('/payment-remove/:id', restaurantController.removePayment)
restaurantRoutes.delete('/remove-type/:id', restaurantController.removeType)
restaurantRoutes.delete('/remove-allergy/:id', restaurantController.removeAllergy)
restaurantRoutes.delete('/remove-booking/:id', restaurantController.removeBooking)
restaurantRoutes.delete('/decline-booking/:id', restaurantController.declineBooking)


//                                                                            food routes

const foodRoutes = express.Router();
foodRoutes.get('/fetch/:id', foodController.fetchFood);
foodRoutes.get('/fetch-all', foodController.fetchAllFood);
foodRoutes.get('/fetch-all-carts', foodController.fetchAllCarts);
foodRoutes.get('/fetch-my-cart/:id', foodController.fetchMyCart);
foodRoutes.get('/fetch-restaurant-cart/:id', foodController.fetchRestaurantCart);
foodRoutes.get('/fetch-category/:id', foodController.fetchCategory);
foodRoutes.get('/fetch-category-with-active-foods/:id', foodController.fetchCategoryWithActiveFoods);
foodRoutes.get('/fetch-package/:id', foodController.fetchPackage);
foodRoutes.get('/fetch-package-active/:id', foodController.fetchPackageActive);
foodRoutes.get('/fetch-category-all', foodController.fetchAllCategory);
foodRoutes.post('/create', foodController.createFood);
foodRoutes.post('/add-to-cart', foodController.addToCart);
foodRoutes.post('/sub-from-cart/:id', foodController.subFromCart);
foodRoutes.post('/create-category', foodController.createCategory);
foodRoutes.post('/create-package', foodController.createPackage);
foodRoutes.put('/update/:id', foodController.updateFood);
foodRoutes.put('/update-package/:id', foodController.updatePackage);
foodRoutes.put('/update-category/:id', foodController.updateCategory);
foodRoutes.put('/add-items/:id', foodController.addFoodToCategory);
foodRoutes.delete('/remove/:id', foodController.removeFood);
foodRoutes.delete('/remove-cart/:id', foodController.removeFromCart);
foodRoutes.delete('/delete-package/:id', foodController.removePackage);
foodRoutes.delete('/remove-category/:id', foodController.removeCategory);


//                                                                            website routes

const websiteRoutes = express.Router();
websiteRoutes.post('/create-caraousel', websiteController.createCaraousel);
websiteRoutes.get('/fetch-caraousels', websiteController.fetchAllCaraousels)
websiteRoutes.delete('/remove-caraousel/:id', websiteController.removeCaraousel)
websiteRoutes.put('/update-carousel/:id', websiteController.updateCaraousel)

//                                                                            order routes

const orderRoutes = express.Router();
orderRoutes.get('/fetch', orderController.fetchAllOrder)
orderRoutes.get('/fetch/:id', orderController.fetchOrder)
orderRoutes.get('/fetch-driver-order/:id', orderController.fetchDriverOrder)
orderRoutes.get('/fetch-by-restaurant/:id', orderController.fetchOrderByRestaurant)
orderRoutes.get('/fetch-by-user/:id', orderController.fetchOrderByUser)
orderRoutes.get('/fetch-driver-pending-orders/:id', orderController.fetchDriverPendingOrders)
orderRoutes.get('/inhouse-by-driver/:id', orderController.fetchInHouseOrderByDrive)
orderRoutes.get('/inhouse-by-owner/:id', orderController.fetchInHouseOrderByOwner)
orderRoutes.get('/fetch-by-owner/:id', orderController.fetchOrderByOnwer)
orderRoutes.get('/update-driver-services-off', orderController.updateDriverServicesOff)
orderRoutes.get('/fetch-by-owner-type/:id', orderController.fetchOrderByOnwerType)
orderRoutes.get('/fetch-pending-owner/:id', orderController.fetchPendingOrderByOnwer)
orderRoutes.get('/fetch-collection', orderController.fetchCollection)
orderRoutes.get('/fetch-all-pending', orderController.fetchAllPendingOrders)
orderRoutes.get('/fetch-recieved-owner/:id', orderController.fetchRecievedOrderByOnwer)
orderRoutes.get('/fetch-by-owner-date/:id', orderController.fetchOrderByOnwerDate)
orderRoutes.get('/fetch-by-driver/:id', orderController.fetchOrderByDriver)
orderRoutes.get('/fetch-by-driver-from-restaurants/:id', orderController.fetchOrderByDriverFromRestaurants)
orderRoutes.get('/fetch-pending-orders-driver/:id', orderController.fetchPendingOrderByDriverFromRestaurants)
orderRoutes.get('/fetch-earning/:id', orderController.fetchDriverEarning)
orderRoutes.post('/create', orderController.createOrder)
orderRoutes.post('/accept-order', orderController.createAcceptOrder)
orderRoutes.post('/pick-up-order', orderController.pickUpOrder)
orderRoutes.post('/deliver-order', orderController.deliverOrder)
orderRoutes.post('/deliver-in-house-order', orderController.deliverInHouseOrder)
orderRoutes.post('/collect-order', orderController.collectOrder)
orderRoutes.post('/accept-order-by-owner', orderController.createAcceptOrderByOwner)
orderRoutes.post('/create-inhouse', orderController.createInHouseOrder)
orderRoutes.put('/update/:id', orderController.updateOrder)
orderRoutes.put('/update-inhouse/:id', orderController.updateInHouse)
orderRoutes.delete('/remove/:id', orderController.removeOrder)
orderRoutes.delete('/remove-drivers/:id', orderController.removeDriverOrder)
orderRoutes.delete('/remove-inhouse/:id', orderController.removeInHouseOrder)


//                                                                            pcf routes

const pcfRoutes = express.Router();
pcfRoutes.get('/fetch-policy', pcfController.fetchAllPolicies);
pcfRoutes.get('/fetch-terms-driver', pcfController.fetchAllTermsDriver);
pcfRoutes.get('/fetch-terms-all', pcfController.fetchAllTerms);
pcfRoutes.get('/fetch-covid-all', pcfController.fetchAllCovid);
pcfRoutes.get('/fetch-faqs-all', pcfController.fetchAllFaqs);
pcfRoutes.get('/fetch-terms-user', pcfController.fetchAllTermsUser);
pcfRoutes.get('/fetch-faq-user', pcfController.fetchAllFaqsUser);
pcfRoutes.get('/fetch-terms-owner', pcfController.fetchAllTermsOwner);
pcfRoutes.get('/fetch-covid-owner', pcfController.fetchCovidOwner);
pcfRoutes.get('/fetch-covid-driver', pcfController.fetchCovidDriver);
pcfRoutes.get('/fetch-faq-owner', pcfController.fetchAllFaqsOwner);
pcfRoutes.get('/fetch-faq-driver', pcfController.fetchAllFaqsDriver);
pcfRoutes.get('/fetch-returns', pcfController.fetchAllReturns);
pcfRoutes.get('/fetch-contacts', pcfController.fetchAllContacts);
pcfRoutes.get('/fetch-contact/:id', pcfController.fetchContactById);
pcfRoutes.post('/create-policy', pcfController.createPolicy);
pcfRoutes.post('/create-terms', pcfController.createTerms);
pcfRoutes.post('/create-faq', pcfController.createFAQ);
pcfRoutes.post('/create-returns', pcfController.createReturn);
pcfRoutes.post('/create-contact', pcfController.createContact);
pcfRoutes.put('/update-policy/:id', pcfController.updatePolicies);
pcfRoutes.put('/update-terms/:id', pcfController.updateTerms);
pcfRoutes.put('/update-faq/:id', pcfController.updateFaq);
pcfRoutes.put('/update-returns/:id', pcfController.updateReturns);
pcfRoutes.delete('/remove-policy/:id', pcfController.removePolicyById);
pcfRoutes.delete('/remove-terms/:id', pcfController.removeTermsById);
pcfRoutes.delete('/remove-faq/:id', pcfController.removeFaqById);
pcfRoutes.delete('/remove-returns/:id', pcfController.removeReturnsById);
pcfRoutes.delete('/remove-contact/:id', pcfController.removeContactsById);



module.exports = {
  adminRoutes,
  pcfRoutes,
  foodRoutes,
  restaurantRoutes,
  orderRoutes,
  superAdminRoutes,
  websiteRoutes
}


// order create title: user_name, description: ordered item1,item2 and item2 type: owner
// restaurant create title: restaurant_created, type: super_admin
// user/owner create title: user_name,  description: created user/onwer account, type: super_admin
// driver create title: user_name,  description: created driver account, type: super_admin


// On Order:
// Stage 1:
// section 1:user will get order confirmation with restaurant name and order id.
// section 2:owner will get confirmation with username and order id.
// Stage 2: user/owner will get notified when driver pickup food with driver/order id.
// Stage 3:order received and delivered msg notification for user and owner

// On Order Cancellation:
// User and owner will get notified about order cancel with order id.
// On Order Delay:
// User and owner will get notified if order is delay.(user:sorry msg, owner: warning msg)