const express = require('express');
const cors = require('cors');
const app = express();
const bodyParser = require('body-parser');
const server = require('http').Server(app);
const connection = require('./config/config')
const jsonParser = bodyParser.json();
const urlencodedParser = bodyParser.urlencoded({ extended: false });
const morgan = require('morgan')
// const socketIO = require('./config/socket')
const socketIo = require("socket.io");
const io = socketIo(server);
const axios = require("axios");
const moment = require("moment")

const { adminRoutes, pcfRoutes, foodRoutes, restaurantRoutes, orderRoutes, superAdminRoutes, websiteRoutes, notificationRoutes } = require('./api/routes/routes');

// mongodb connection
connection;
// socketIO



function getBaseUrl() {
	if (process.env.NODE_ENV === "prod") {
		return "https://api.foodapps.uk/api/"
	} else if (process.env.NODE_ENV === "uat") {
		return "https://api-uat.foodapps.uk/api/"
	} else if (process.env.NODE_ENV === "local-prod" || process.env.NODE_ENV === "local-uat" || process.env.NODE_ENV === "local") {
		return `http://localhost:${process.env.PORT}/api/`
	}
}

const getApiAndEmit = async (socket, id) => {

	const baseURL = getBaseUrl();
	try {
		const date = new Date;

		const orders = await axios.get(
			`${baseURL}order/fetch`
		);
		const ordersData = orders.data.data
		ordersData.forEach(async element => {
			var a = moment(date);
			var b = moment(element.createdAt);
			const diff = a.diff(b, 'days')
			if (diff > 7) {
				await axios.delete(
					`${baseURL}order/remove/${element._id}`
				);
			}
		});

		const ordersDriverServicesOff = await axios.get(
			`${baseURL}order/update-driver-services-off`
		);

		const pendingOrders = await axios.get(
			`${baseURL}order/fetch-all-pending`
		);
		const pendingOrdersData = pendingOrders.data.data
		pendingOrdersData.forEach(async element => {
			var a = moment(date);
			var b = moment(element.createdAt);
			const diff = a.diff(b, 'minutes')
			if (diff > 5) {
				await axios.delete(
					`${baseURL}order/remove/${element._id}`
				);
			}
		});

		const collectionOrders = await axios.get(
			`${baseURL}order/fetch-collection`
		);
		const collectionOrdersData = collectionOrders.data.data
		collectionOrdersData.forEach(async element => {
			const pickUpTime = moment(element.recievedTime, 'h:mm A')
				.add(element.restaurant.pickupTime, 'minutes')
				.format('h:mm A');
			var a = moment(new Date);
			var b = moment(element.createdAt);
			const diff = a.diff(b, 'minutes')
			if (diff > element.restaurant.pickupTime) {
				const orderData = {
					orderId: element._id
				}
				await axios.post(
					`${baseURL}order/collect-order`, orderData
				);
			}
		});

		const bookings = await axios.get(
			`${baseURL}restaurant/fetch-booking`
		);
		const bookingsData = bookings.data.data
		bookingsData.forEach(async element => {
			var a = moment(date);
			var b = moment(element.createdAt);
			const diff = a.diff(b, 'days')
			if (diff > 7) {
				await axios.delete(
					`${baseURL}restaurant/remove-booking/${element._id}`
				);
			}
		});

		const pendingBookings = await axios.get(
			`${baseURL}restaurant/fetch-pending-booking`
		);
		const pendingBookingsData = pendingBookings.data.data
		pendingBookingsData.forEach(async element => {
			var a = moment(date);
			var b = moment(element.createdAt);
			const diff = a.diff(b, 'days')
			if (diff > 0) {
				await axios.delete(
					`${baseURL}restaurant/remove-booking/${element._id}`
				);
			}
		});

		const notifications = await axios.get(
			`${baseURL}superadmin/notifications`
		);
		const notificationsData = notifications.data.data
		notificationsData.forEach(async element => {
			var a = moment(date);
			var b = moment(element.createdAt);
			const diff = a.diff(b, 'days')
			if (diff > 0) {
				await axios.delete(
					`${baseURL}superadmin/notification/${element._id}`
				);
			}
		});
	} catch (error) {
		console.error(`Catched Error: ${error}`);
	}
};

interval = setInterval(() => getApiAndEmit(), 36000);
// interval = setInterval(() => getApiAndEmit(), 5000);

app.use(cors({
	'Access-Control-Allow-Origin': '*'
}));

app.use(morgan('combined'))
app.use(jsonParser);
app.use(express.json({ extended: false }));
app.use(urlencodedParser);
app.use(express.static(__dirname));

app.use('/api/admin', adminRoutes);
app.use('/api/superadmin', superAdminRoutes);
app.use('/api/pcf', pcfRoutes);
app.use('/api/food', foodRoutes);
app.use('/api/restaurant', restaurantRoutes);
app.use('/api/order', orderRoutes);
app.use('/api/website', websiteRoutes);
// app.use('/api/notification', notificationRoutes);

//            PING APIS 
app.get('/', function (req, res) {
	res.status(200).send('Version: 1.0')
})
app.get('/api', function (req, res) {
	res.status(200).send('hello api')
})
app.get('/api/admin', function (req, res) {
	res.status(200).send('hello admin')
})

const PORT = process.env.PORT || 2020;
server.listen(PORT, () => console.log('listening port ' + PORT));
// uploading this file to github