
const PROD_DB_URL = 'mongodb+srv://root:foodapps123@cluster0-ycbet.mongodb.net/foodapps?retryWrites=true&w=majority'
const UAT_DB_URL = 'mongodb+srv://root:foodapps123@cluster0-ycbet.mongodb.net/foodapps?retryWrites=true&w=majority'
const LOCAL_DB_URL = 'mongodb://localhost:27017/foodapps'

function getDbURL() {
	if (process.env.NODE_ENV === "prod" || process.env.NODE_ENV === "local-prod") {
		return PROD_DB_URL
	} else if (process.env.NODE_ENV === "uat" || process.env.NODE_ENV === "local-uat") {
		return UAT_DB_URL
	} else if (process.env.NODE_ENV === "local") {
		return LOCAL_DB_URL
	}
}

const config = 'mysecrettoken';

module.exports = {
	CLOUD_URL: getDbURL(),
	config
} 