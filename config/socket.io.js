


function getBaseUrl() {
    if (process.env.NODE_ENV === "prod") {
        return "https://api.foodapps.uk/api/"
    } else if (process.env.NODE_ENV === "uat") {
        return "https://api-uat.foodapps.uk/api/"
    } else if (process.env.NODE_ENV === "local-prod" || process.env.NODE_ENV === "local-uat" || process.env.NODE_ENV === "local") {
        return `http://localhost:${process.env.PORT}/api/`
    }
}

const getApiAndEmit = async (socket, id) => {

    const baseURL = getBaseUrl();
    try {

        const orders = await axios.get(
            `${baseURL}order/fetch`
        );

        // console.log(orders.data.data)
    } catch (error) {
        console.error(`Catched Error: ${error}`);
    }
};

interval = setInterval(() => getApiAndEmit(), 2000);